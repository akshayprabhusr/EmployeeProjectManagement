<!DOCTYPE html>
<html>
<head>

<style>
form {
    border: 3px solid #f1f1f1;
}
.loginContainer {
    float:right;
    padding: 16px 20px;
    width:25%;
    margin-right: 35%;
    margin-top: 5%;
}

input  {
    width: 100% !important;
    padding: 12px 20px !important;
    margin: 8px 0 !important;
    display: inline-block !important; 
    border: 1px solid #ccc !important;
    box-sizing: border-box !important;
}

button:hover {
    opacity: 0.8;
}
button {
    background-color: #488AC7;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%
}
body{ 
  margin:0 auto;
  background-image:url("./images/logo.png");
  background-size: 90% 180%;
  background-repeat: no-repeat;
  background-position: unset; 
  background-color:#C0C0C0;
   
 }

</style>
</head>

<body>

<form action="Login" method="get">
  <div class="loginContainer">
   <h2 align="center">Login Form</h2>
    <label><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="username" required>

    <label><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>
        
    <button type="submit">Login</button>
  </div>
  <input type="hidden" name="action" value="Login">
</form>

</body>
</html>

