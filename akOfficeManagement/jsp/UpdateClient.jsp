<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="akstyle.css">
<style>
input[type=text], input[type=email] {
    width: 100% !important;
    padding: 12px 20px !important;
    margin: 8px 0 !important;
    display: inline-block !important; 
    border: 1px solid #ccc !important;
    box-sizing: border-box !important;
}
.create{
    display:none;
}
</style>
</head>

<body>
<jsp:include page="Header.jsp" /> <br><br>

<form action="Client" style="border:1px solid #ccc">
  <div class="container" align="left">
    <label><b>Name</b></label><br>
    <input type="text" placeholder="Enter Name" name="clientName" value="${client.name}" required pattern="[a-z A-Z]{1,}"><br>

    <label><b>Organization</b></label><br>
    <input type="text" placeholder="Enter Organization" name="org" value="${client.org}" required pattern="[a-z A-Z0-9]{1,}"><br>
    
    <label><b>Email</b></label><br>
    <input type="text" placeholder="Enter Email" name="clientEmailId" value="${client.emailId}" required pattern="\S*[^@][@]{1}[a-zA-Z]{1,}[.]{1}[a-zA-Z]{1,}"><br>
    <input type="hidden" name="action" value="update">
    <input type="hidden" name="clientId" value="${client.id}"">
    <button type="submit" class="signupbtn">Update</button>
    <button type="reset" class="cancelbtn">Cancel</button>
    </div>
</form>

<jsp:include page="Footer.jsp" />
</body>
</html>
