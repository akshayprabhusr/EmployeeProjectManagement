<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="akstyle.css">
<style>
input[type=text], input[type=email] {
    width: 100% !important;
    padding: 12px 20px !important;
    margin: 8px 0 !important;
    display: inline-block !important; 
    border: 1px solid #ccc !important;
    box-sizing: border-box !important;
}
.create{
    display:none;
}
</style>
</head>

<body>
<jsp:include page="Header.jsp" /> <br><br>

<form action="Employee" method="get" style="border:1px solid #ccc" id="employeeMenu">
  <div class="container" align="left" style="overflow:auto;">
    <label><b>Name</b></label><br>
    <input type="text" placeholder="Enter Name" value="${employee.name}" name="empName" required pattern="[a-z A-Z]{1,}"><br>

    <label><b>Date of Birth</b></label><br>
    <input type="text" placeholder="Enter Date of Birth (01/01/1995)" value="${employee.dob}" name="dob" required pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}"><br>
    
    <label><b>Mobile Number</b></label><br>
    <input type="text" placeholder="Enter Mobile Number" name="mobileNumber" value="${employee.mobileNumber}" required pattern="[789]{1}[0-9]{9}"><br>
    
    <label><b>Email</b></label><br>
    <input type="email" placeholder="Enter Email" name="empEmailId" value="${employee.emailId}" required pattern="\S*[^@][@]{1}[a-zA-Z]{1,}[.]{1}[a-zA-Z]{1,}"><br>
    <label><b>Projects</b></label><br>
    
    <div class="container" style="overflow:scroll; width:350px; height:50px">
    <c:forEach items="${projects}" var="project">
     <input type="checkbox" name="projects" id="${project.id}" value="PROJ${project.id}">${project.title}<br>
    </c:forEach> 
    <c:forEach items="${employee.projects}" var="project">
     <input type="checkbox" name="projects" id="${project.id}" value="PROJ${project.id}" checked="checked">${project.title}<br>
    </c:forEach>
    
</div>
    
    <input type="hidden" name="action" value="Update">
    <input type="hidden" name="empId" value="${employee.id}"">
    <button type="submit" class="signupbtn">Update</button>
    <button type="reset" class="cancelbtn">Cancel</button>
    </div>
</form>

<jsp:include page="Footer.jsp" />
</body>
</html>
