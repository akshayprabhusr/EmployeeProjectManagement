<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="akstyle.css">
<style>
input[type=text], input[type=email], select, textarea {
    width: 100% !important;
    padding: 12px 20px !important;
    margin: 8px 0 !important;
    display: inline-block !important; 
    border: 1px solid #ccc !important;
    box-sizing: border-box !important;
}
.create{
    display:none;
}
</style>
</head>

<body>
<jsp:include page="Header.jsp" /> <br><br>

<form action="Project" style="border:1px solid #ccc">
    <div class="container" align="left">
    <label><b>Client*</b></label><br>
    <select name="clientId">
    <c:forEach items="${clients}" var="client">
     <option value="${client.id}">CT${client.id}</option>
    </c:forEach>   
    <option value="" selected disabled hidden>CT${project.client.id}</option>
</select>
  
    <label><b>Title*</b></label><br>
    <input type="text" placeholder="Enter title" name="title" value="${project.title}" required pattern="[a-zA-Z0-9 ]{1,}"><br>

    <label><b>Domain*</b></label><br>
    <input type="text" placeholder="Enter domain" name="domain" value="${project.domain}" required pattern="[a-z A-Z]{1,}"><br>
    </div>
    <div class="container" align="left">
    <label><b>Description</b></label>
    <textarea placeholder="Enter description..." name="description" rows="5" 
    cols=25% >${project.description}</textarea>
      <button type="submit" class="signupbtn">Update</button>
      <button type="reset" class="cancelbtn">Cancel</button>
      <input type="hidden" name="action" value="update">
      <input type="hidden" name="projId" value="${project.id}"">
    </div>
</div>
</form>

<jsp:include page="Footer.jsp" />
</body>
</html>
