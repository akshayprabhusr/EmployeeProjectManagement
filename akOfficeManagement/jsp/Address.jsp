<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="akstyle.css">
<style>
input[type=text], input[type=email] {
    width: 100% !important;
    padding: 12px 20px !important;
    margin: 8px 0 !important;
    display: inline-block !important; 
    border: 1px solid #ccc !important;
    box-sizing: border-box !important;
}
</style>
</head>
<body>
<jsp:include page="Header.jsp" />

<form action="/action_page.php" style="border:1px solid #ccc">
  <div class="container" align="left">
    <label><b>Door No</b></label><br>
    <input type="text" placeholder="Enter Door No" name="doorNo" required pattern="[a-zA-Z0-9]{1,}"><br>

    <label><b>Street</b></label><br>
    <input type="text" placeholder="Enter street" name="street" required pattern="[a-zA-Z0-9]{1,}"><br>
    
    <label><b>Area</b></label><br>
    <input type="text" placeholder="Enter area" name="area" required pattern="[a-zA-Z0-9]{1,}"><br>
    
    <label><b>District</b></label><br>
    <input type="text" placeholder="Enter district" name="district" required pattern="[a-zA-Z]{1,}"><br>
    
    <label><b>Pincode</b></label><br>
    <input type="text" placeholder="Enter pincode" name="pincode" required pattern="[a-zA-Z]{1,}"><br>
    
    <label><b>State</b></label><br>
    <input type="text" placeholder="Enter state" name="state" required pattern="[a-zA-Z]{1,}"><br>

    <div class="clearfix">
      <button type="reset" class="cancelbtn">Cancel</button>
      <button type="submit" class="signupbtn">Create</button>
    </div>
  </div>
</form>
<jsp:include page="Footer.jsp" />
</body>
</html>

