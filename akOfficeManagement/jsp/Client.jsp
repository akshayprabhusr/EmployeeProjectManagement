<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="akstyle.css">
<style>
input[type=text], input[type=email] {
    width: 100% !important;
    padding: 12px 20px !important;
    margin: 8px 0 !important;
    display: inline-block !important; 
    border: 1px solid #ccc !important;
    box-sizing: border-box !important;
}
.create{
    display:none;
}
</style>
</head>
<body>
<jsp:include page="Header.jsp" /> <br><br>
<button class="right" onclick="myClientFunction()">Add new Client</button>
<form action="Client">
    <button type="submit">Show All Clients</button>
    <input type="hidden" name="action" value="displayAll">
</form>

<script>
function myClientFunction() {
    var x = document.getElementById("createClient");
    var y = document.getElementById("displayClients");
    if (x.style.display == "block") {
        x.style.display = "none";
        y.style.display = "block";
    } else {
        x.style.display = "block";
        y.style.display = "none";
    }
}
</script>
<div id="createClient" class="create" style="overflow:auto;">

<form action="Client" style="border:1px solid #ccc">
  <div class="container" align="left">
    <label><b>Name*</b></label><br>
    <input type="text" placeholder="Enter Name" name="clientName" required pattern="[a-z A-Z]{1,}"><br>

    <label><b>Organization*</b></label><br>
    <input type="text" placeholder="Enter Organization" name="org" required pattern="[a-z A-Z0-9]{1,}"><br>
    
    <label><b>Email*</b></label><br>
    <input type="text" placeholder="Enter Email" name="clientEmailId" required pattern="\S*[^@][@]{1}[a-zA-Z]{1,}[.]{1}[a-zA-Z]{1,}"><br>
   </div>
    <div class="container" align="left">
    <label><h4><b>Address</b></h4></label>
    <label><b>Door No*</b></label><br>
    <input type="text" placeholder="Enter Door No" name="doorNo" required pattern="[a-zA-Z0-9]{1,}"><br>

    <label><b>Street</b></label><br>
    <input type="text" placeholder="Enter street" name="street" pattern="[a-zA-Z0-9]{1,}"><br>
    
    <label><b>Area*</b></label><br>
    <input type="text" placeholder="Enter area" name="area" required pattern="[a-zA-Z0-9]{1,}"><br>
    <div class="clearfix"> 
      <button type="submit" class="signupbtn">Create</button>
      <button type="reset" class="cancelbtn">Cancel</button>
    </div>
    </div>
    <div class="container" align="left">
    <br><br><br>
    <label><b>District*</b></label><br>
    <input type="text" placeholder="Enter district" name="district" required pattern="[a-zA-Z]{1,}"><br>
    
    <label><b>Pincode*</b></label><br>
    <input type="text" placeholder="Enter pincode" name="pincode" required pattern="[a-zA-Z]{1,}"><br>
    
    <label><b>State*</b></label><br>
    <input type="text" placeholder="Enter state" name="state" required pattern="[a-zA-Z]{1,}"><br>
    <input type="hidden" name="action" value="addClient">
    </div>
</div>

<div id="displayClients" class="table-container" style="overflow-x:auto;">
  <table>
    <tr>
      <th>Client Id</th>
      <th>Name</th>
      <th>Organization</th>
      <th>Email-Id</th>
      <th colspan="3">Action</th>
    </tr>
    <c:forEach items="${clients}" var="client">
        <tr>
            <td>CT${client.id}</td>
            <td>${client.name}</td>  
            <td>${client.org}</td>
            <td>${client.emailId}</td>
            </td>
            <td>
            <a href="Client?action=display&clientId=${client.getId()}" data-toggle="tooltip"title="view">
                <img src="../images/view.png"
                style="width:20px;height:20px;">
            </a></td>
            <td>
            <a href="Client?action=toUpdate&clientId=${client.getId()}"data-toggle="tooltip"title="view">
                <img src="../images/edit2.jpg"
                style="width:20px;height:20px;">
            </a></td>
            <td>
            <a href="Client?action=delete&clientId=${client.getId()}"data-toggle="tooltip"title="delete"
            onclick="return confirm('Are you sure you want to delete this Client ?');">
                <img src="../images/delete2.jpg"
                style="width:20px;height:20px;">
            </a>
            </td>
        </tr>
    </c:forEach>    
  </table>
</div>
<jsp:include page="Footer.jsp" />
</body>
</html>

