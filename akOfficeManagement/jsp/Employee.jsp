<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="akstyle.css">
<style>
input[type=text], input[type=email] {
    width: 100% !important;
    padding: 12px 20px !important;
    margin: 8px 0 !important;
    display: inline-block !important; 
    border: 1px solid #ccc !important;
    box-sizing: border-box !important;
}
.create{
    display:none;
}
</style>
</head>

<body>
<jsp:include page="Header.jsp" /> <br><br>
<button id="createButton" onclick="myEmpFunction()">Add new Employee</button>
<form action="Employee" id="displayButton">
    <button type="submit" onclick="myEmpFunction()">Show All Employees</button>
    <input type="hidden" name="action" value="displayAll">
</form>

<script>
function myEmpFunction() {
    var a = document.getElementById("createEmployee");
    var b = document.getElementById("displayEmployees");
    var c = document.getElementById("createButton");
    if (a.style.display == "block") {
        a.style.display = "none";
        b.style.display = "block";
        c.style.display = "block";
    } else {
        a.style.display = "block";
        b.style.display = "none";
        c.style.display = "none";
    }
    
    
}
</script>
<div id="createEmployee" class="create" style="overflow:auto;">

<form action="Employee" method="get" style="border:1px solid #ccc" id="employeeMenu">
  <div class="container" align="left">
    <label><b>Name</b></label><br>
    <input type="text" placeholder="Enter Name" name="empName" required pattern="[a-z A-Z]{1,}"><br>

    <label><b>Date of Birth</b></label><br>
    <input type="text" placeholder="Enter Date of Birth (01/01/1995)" name="dob" required pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}"><br>
    
    <label><b>Mobile Number</b></label><br>
    <input type="text" placeholder="Enter Mobile Number" name="mobileNumber" required pattern="[789]{1}[0-9]{9}"><br>
    
    <label><b>Email</b></label><br>
    <input type="email" placeholder="Enter Email" name="empEmailId" required pattern="\S*[^@][@]{1}[a-zA-Z]{1,}[.]{1}[a-zA-Z]{1,}"><br>
    </div>
    <div class="container" align="left">
    <label><h4><b>Address</b></h4></label>
    <label><b>Door No</b></label><br>
    <input type="text" placeholder="Enter Door No" name="doorNo" required pattern="[a-zA-Z0-9]{1,}"><br>

    <label><b>Street</b></label><br>
    <input type="text" placeholder="Enter street" name="street" required pattern="[a-zA-Z0-9]{1,}"><br>
    
    <label><b>Area</b></label><br>
    <input type="text" placeholder="Enter area" name="area" required pattern="[a-zA-Z0-9]{1,}"><br>
    <div class="clearfix"> 
      <button type="submit" class="signupbtn">Create</button>
      <button type="reset" class="cancelbtn">Cancel</button>
    </div>
    </div>
    <div class="container" align="left">
    <br><br><br>
    <label><b>District</b></label><br>
    <input type="text" placeholder="Enter district" name="district" required pattern="[a-zA-Z]{1,}"><br>
    
    <label><b>Pincode</b></label><br>
    <input type="text" placeholder="Enter pincode" name="pincode" required pattern="[0-9]*" maxlength="6"><br>
    
    <label><b>State</b></label><br>
    <input type="text" placeholder="Enter state" name="state" required pattern="[a-zA-Z]{1,}"><br>
    <input type="hidden" name="action" value="addEmployee">
    </div>
</div>
</form>
<div id="displayEmployees" class="table-container display" style="overflow-x:auto;" >
  <table>
    <tr>
      <th>Employee Id</th>
      <th>Name</th>
      <th>Date of Birth</th>
      <th>Email-Id</th>
      <th>Mobile Number</th>
      <th colspan="3">Action</th>
    </tr>
    <c:forEach items="${employees}" var="employee">
        <tr>
            <td>I2IT${employee.id}</td>
            <td>${employee.name}</td>  
            <td>${employee.dob}</td>
            <td>${employee.emailId}</td>
            <td>${employee.mobileNumber}</td> 
            <td>
            <a href="Employee?action=display&empId=${employee.getId()}"data-toggle="tooltip"title="view" >
                <img src="../images/view.png"
                style="width:20px;height:20px;">
            </a></td>
            <td>
            <a href="Employee?action=toUpdate&empId=${employee.getId()}"data-toggle="tooltip"title="edit">
                <img src="../images/edit2.jpg"
                style="width:20px;height:20px;">
            </a></td>
            <td>
            <a href="Employee?action=delete&empId=${employee.getId()}"data-toggle="tooltip"title="delete" onclick="return confirm('Are you sure you want to delete this Employee ?');">
                <img src="../images/delete2.jpg"
                style="width:20px;height:20px;">
            </a>
            </td>
        </tr>
    </c:forEach>    
  </table>
</div>

<jsp:include page="Footer.jsp" />
</body>
</html>

