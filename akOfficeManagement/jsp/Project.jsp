<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="akstyle.css">
<style>
input[type=text], input[type=email], select, textarea {
    width: 100% !important;
    padding: 12px 20px !important;
    margin: 8px 0 !important;
    display: inline-block !important; 
    border: 1px solid #ccc !important;
    box-sizing: border-box !important;
}
.create{
    display:none;
}
</style>
</head>

<body>
<jsp:include page="Header.jsp" /> <br><br>
<button class="right" onclick="myProjFunction()">Add new Project</button>
<form action="Project">
    <button type="submit" onclick="myFunction()">Show All Projects</button>
    <input type="hidden" name="action" value="displayAll">
</form>

<script>
function myProjFunction() {
    var x = document.getElementById("createProject");
    var y = document.getElementById("displayProjects");
    if (x.style.display == "block") {
        x.style.display = "none";
        y.style.display = "block";
    } else {
        x.style.display = "block";
        y.style.display = "none";
    }
}
</script>
<div id="createProject" class="create">
<form action="Project" style="border:1px solid #ccc">
    <div class="container" align="left">
    <label><b>Client*</b></label><br>
    <select name="clientId">
    <c:forEach items="${clients}" var="client">
     <option value="${client.id}">CT${client.id}</option>
    </c:forEach>   
    <option value="" selected disabled hidden>Choose Client</option>
</select>
  
    <label><b>Title*</b></label><br>
    <input type="text" placeholder="Enter title" name="title" required pattern="[a-zA-Z0-9 ]{1,}"><br>

    <label><b>Domain*</b></label><br>
    <input type="text" placeholder="Enter domain" name="domain" required pattern="[a-z A-Z]{1,}"><br>
    </div>
    <div class="container" align="left"><br>
    <label><b>Description</b></label>
    <textarea placeholder="Enter description..." name="description" rows="5" ></textarea> 
      <button type="reset" class="cancelbtn">Cancel</button>
      <button type="submit" class="signupbtn">Create</button>
      <input type="hidden" name="action" value="addProject">
    </div>
</div>
</form>
<div id="displayProjects" class="table-container" style="overflow:auto;">
  <table>
    <tr>
      <th>Project Id</th>
      <th>Title</th>
      <th>Domain</th>
      <th>Description</th>
      <th colspan="3">Action</th>
    </tr>
    <c:forEach items="${projects}" var="project">
        <tr>
            <td>PROJ${project.id}</td>
            <td>${project.title}</td>  
            <td>${project.domain}</td>
            <td>${project.description}</td>
            <td>
            <a href="Project?action=display&projId=${project.getId()}" data-toggle="tooltip"title="view">
                <img src="../images/view.png"
                style="width:20px;height:20px;">
            </a></td>
            <td>
            <a href="Project?action=toUpdate&projId=${project.getId()}"data-toggle="tooltip"title="edit">
                <img src="../images/edit2.jpg"
                style="width:20px;height:20px;">
            </a></td>
            <td>
            <a href="Project?action=delete&projId=${project.getId()}"data-toggle="tooltip"title="delete"
            onclick="return confirm('Are you sure you want to delete this Project ?');">
                <img src="../images/delete2.jpg"
                style="width:20px;height:20px;">
            </a>
            </td>
        </tr>
    </c:forEach>    
  </table>
</div>
<jsp:include page="Footer.jsp" />
</body>
</html>

