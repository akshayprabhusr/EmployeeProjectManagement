<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="akstyle.css">
</head>
<body>
<jsp:include page="Header.jsp" />
<div class="table-container" style="overflow-x:auto;">
  <table>
    <tr>
      <th>Project Id</th>
      <th>Title</th>
      <th>Domain</th>
      <th>Description</th>
    </tr>
        <tr>
            <td>PROJ${project.id}</td>
            <td>${project.title}</td>  
            <td>${project.domain}</td>
            <td>${project.description}</td>
        </tr>
  </table>
</div>
 <h3>Client</h3>
<div class="table-container" style="overflow-x:auto;">
  <table>
    <tr>
      <th>Client Id</th>
      <th>Name</th>
      <th>Organization</th>
      <th>Email-Id</th>
    </tr>
        <tr>
            <td>CT${project.client.id}</td>
            <td>${project.client.name}</td>  
            <td>${project.client.org}</td>
            <td>${project.client.emailId}</td>
        </tr>
  </table>
</div>

<div class="table-container" style="overflow-x:auto;">
  <table>
    <tr>
      <th>Employee Id</th>
      <th>Name</th>
      <th>Date of Birth</th>
      <th>Email-Id</th>
      <th>Mobile Number</th>
    </tr>
    <h3>Employees</h3>
    <c:forEach items="${project.employees}" var="employee">
        <tr>
            <td>I2IT${employee.id}</td>
            <td>${employee.name}</td>  
            <td>${employee.dob}</td>
            <td>${employee.emailId}</td>
            <td>${employee.mobileNumber}</td> 
            </td>
        </tr>
    </c:forEach>    
  </table>
</div>
<jsp:include page="Footer.jsp" />
</body>
</html>
