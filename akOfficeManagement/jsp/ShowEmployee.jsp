<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
</head>
<body>
<jsp:include page="Header.jsp" />
<div class="table-container" style="overflow-x:auto;">
  <table>
    <tr>
      <th>Employee Id</th>
      <th>Name</th>
      <th>Date of Birth</th>
      <th>Email-Id</th>
      <th>Mobile Number</th>
    </tr>
        <tr>
            <td>I2IT${employee.id}</td>
            <td>${employee.name}</td>  
            <td>${employee.dob}</td>
            <td>${employee.emailId}</td>
            <td>${employee.mobileNumber}</td> 
            </td>
        </tr>
  </table>
</div>
<div class="table-container" style="overflow-x:auto;">
  <table>
    <tr>
      <th>Door No</th>
      <th>Street</th>
      <th>Area</th>
      <th>District</th>
      <th>Pincode</th>
      <th>State</th>
    </tr>
    <h3>Address</h3>
    <c:forEach items="${employee.addresses}" var="address">
        <tr>
            <td>${address.doorNo}</td>
            <td>${address.street}</td>  
            <td>${address.area}</td>
            <td>${address.district}</td>
            <td>${address.pinCode}</td> 
            <td>${address.state}</td>
            </td>
        </tr>
    </c:forEach>    
  </table>
</div>

<div class="table-container" style="overflow-x:auto;">
  <table>
    <tr>
      <th>Project Id</th>
      <th>Title</th>
      <th>Domain</th>
      <th>Description</th>
    </tr>
    <h3>Projects</h3>
    <c:forEach items="${employee.projects}" var="project">
         <tr>
            <td>PROJ${project.id}</td>
            <td>${project.title}</td>  
            <td>${project.domain}</td>
            <td>${project.description}</td>
        </tr>
    </c:forEach>    
  </table>
</div>

<jsp:include page="Footer.jsp" />
</body>
</html>
