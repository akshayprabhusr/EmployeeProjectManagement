<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="akstyle.css">
</head>
<body>
<jsp:include page="Header.jsp" />
<div class="table-container" style="overflow-x:auto;">
  <table>
    <tr>
      <th>Client Id</th>
      <th>Name</th>
      <th>Organization</th>
      <th>Email-Id</th>
    </tr>
        <tr>
            <td>CT${client.id}</td>
            <td>${client.name}</td>  
            <td>${client.org}</td>
            <td>${client.emailId}</td>
        </tr>
  </table>
</div>
<div class="table-container" style="overflow-x:auto;">
  <table>
    <tr>
      <th>Door No</th>
      <th>Street</th>
      <th>Area</th>
      <th>District</th>
      <th>Pincode</th>
      <th>State</th>
    </tr>
    <h3>Address</h3>
    <c:forEach items="${client.addresses}" var="address">
        <tr>
            <td>${address.doorNo}</td>
            <td>${address.street}</td>  
            <td>${address.area}</td>
            <td>${address.district}</td>
            <td>${address.pinCode}</td> 
            <td>${address.state}</td>
            </td>
        </tr>
    </c:forEach>    
  </table>
</div>
<jsp:include page="Footer.jsp" />
</body>
</html>
