package hibernate;
 
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.HibernateException;

public class HibernateSessionFactory {
    private static SessionFactory sessionFactory;

    private static SessionFactory createSessionFactory() {
        try {
            Configuration configuration = new Configuration();
            System.out.println("Hibernate Configuration loaded");
            sessionFactory = configuration.configure("hibernate.cfg.xml").buildSessionFactory();
            return sessionFactory;
        } catch (HibernateException ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            ex.printStackTrace();
            return null;
        }
    }

    public static SessionFactory getSessionFactory() {
        if(sessionFactory == null) {
            sessionFactory = createSessionFactory();
        }
        return sessionFactory;
    }
}
