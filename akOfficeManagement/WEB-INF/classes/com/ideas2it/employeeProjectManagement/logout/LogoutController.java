package com.ideas2it.employeeProjectManagement.logout;
 
import java.io.IOException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutController extends HttpServlet {
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException { 
        try {  
            if (request.getParameter("action").equals("logout")) {
        		HttpSession session = request.getSession(false);
	            session.removeAttribute("username");
	            session.removeAttribute("password");
                session.invalidate();
                RequestDispatcher rd = request.getRequestDispatcher("Login.jsp");
                rd.forward(request, response);
            }
        } catch (IOException e) {
             System.out.println("Operation failed" + e);    
        }
    }
}
