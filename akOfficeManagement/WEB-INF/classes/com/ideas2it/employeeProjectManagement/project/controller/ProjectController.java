package com.ideas2it.employeeProjectManagement.project.controller;

import java.io.IOException;
import java.lang.Boolean;
import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ideas2it.employeeProjectManagement.common.Constants;
import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.client.service.ClientService;
import com.ideas2it.employeeProjectManagement.client.service.impl.ClientServiceImpl;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.employee.service.EmployeeService;
import com.ideas2it.employeeProjectManagement.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.model.Project;
import com.ideas2it.employeeProjectManagement.project.service.ProjectService;
import com.ideas2it.employeeProjectManagement.project.service.impl.ProjectServiceImpl;
import com.ideas2it.employeeProjectManagement.util.ValidationUtil;

/**
 * Class to interact with user and redirect flow to service layer to perform
operations on Project.
 *
 * @author  Akshay Prabhu S R
**/
public class ProjectController extends HttpServlet {
    private ClientService clientService = new ClientServiceImpl();
    private EmployeeService employeeService = new EmployeeServiceImpl();
    private ProjectService projectService = new ProjectServiceImpl();
    
    public void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        displayProjects(request, response);           
    } 
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) throws ServletException, IOException {  
        switch (request.getParameter("action")) {
            case "addProject":
                addProject(request, response);
                break;
            case "displayAll":
                displayProjects(request, response);
                break;
            case "display":
                displayProject(request, response);
                break;
            case "delete":
                deleteProject(request, response);
                break;
            case "toUpdate":
                getProjectToUpdate(request, response);
                break;
            case "update":
                updateProject(request, response);
                break;
            default :
                break;            
        }    
    }
    
    private void deleteProject(HttpServletRequest request,
            HttpServletResponse response) {
                        System.out.println(request);
        try {
            projectService.removeProjectById("PROJ" + request.getParameter("projId"));
            request.setAttribute("projects", projectService.getAllProjects());
            request.setAttribute("clients", clientService.getAllClients());
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/Project.jsp");
            rd.forward(request, response);
        } catch (ServletException e) {
            System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }    
    }
    
    private void displayProjects(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        try {    
            request.setAttribute("projects", projectService.getAllProjects());
            request.setAttribute("clients", clientService.getAllClients());
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/Project.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }
    
    }
    
    private void addProject(HttpServletRequest request,
            HttpServletResponse response) {
        try {  
            Project project = new Project();
            project.setTitle(request.getParameter("title"));
            project.setDomain(request.getParameter("domain"));
            project.setDescription(request.getParameter("description"));
            project.setClient(clientService.getClientById(request.getParameter("clientId")));
            projectService.createProject(project); 
            request.setAttribute("clients", clientService.getAllClients());
            request.setAttribute("projects", projectService.getAllProjects());
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/Project.jsp");
            rd.forward(request, response);   
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }   
    }
    
    private void getProjectToUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        try {
            Project project = projectService.getProjectById(request.getParameter("projId"));
            request.setAttribute("project",project);       
            request.setAttribute("clients", clientService.getAllClients());   
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/UpdateProject.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }       
    }
    
    private void updateProject(HttpServletRequest request,
            HttpServletResponse response) {
        try {
            Project project = projectService.getProjectById(request.getParameter("projId"));
            project.setTitle(request.getParameter("title"));
            project.setDomain(request.getParameter("domain"));
            project.setDescription(request.getParameter("description"));
            projectService.modifyProject(project); 
            request.setAttribute("projects", projectService.getAllProjects());          
            request.setAttribute("clients", clientService.getAllClients());
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/Project.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }       
    }
    
    private void displayProject(HttpServletRequest request,
            HttpServletResponse response) {
        try {
            Project project = projectService.getProjectById(request.getParameter("projId"));
            request.setAttribute("project", project);  
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/ShowProject.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }   
    }
}
