package com.ideas2it.employeeProjectManagement.project.model;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;

/**
 * Class to maintain Project details
 * Contains getters and setters to get and set Project details 
 * @author Akshay Prabhu S R 
 */
public class Project {     
    private boolean isDeleted = Boolean.FALSE ;
    private int id;   
    private String title;
    private String domain;
    private String description;
    private String projId; 
    private Client client;   
    private List<Employee> employees; 
   
    public Project(String title,String domain,String description) {
        this.title = title;
        this.domain = domain; 
        this.description = description;
    }

    public Project(String title,String domain,String description,String projId) {
        this.title = title;
        this.domain = domain; 
        this.description = description;
        this.projId = projId;
    }
  
    public Project() {
    }
    
    /**
     * Getters and setters
     */
    public boolean getIsDeleted() {
        return this.isDeleted;
    }
 
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    } 
    
    public int getId() {
        return this.id;
    }
 
    public void setId(int id) {
        this.id = id;
    } 
    
    public String getProjId() {
        return this.projId;
    }
 
    public void setProjId(String projId) {
        this.projId = projId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getDomain() {
        return this.domain;
    }   

    public void setDomain(String domain) {
        this.domain = domain;
    }    
     
    
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }  
    
    public Client getClient() {
        return this.client;
    }   

    public void setClient(Client client) {
        this.client = client;
    }  
    
    public List<Employee> getEmployees() {
        return this.employees;
    }
    
    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }  
    
    public void addEmployee(Employee employee) {
        this.employees.add(employee);
    }
    
    public void removeEmployee(Employee employee) {
        this.employees.remove(employee);
    }
    
    /*
     * Overrides the toString() method and prints the project details
     */
    public String toString() {
        return "\n\t\tProject ID      = PROJ" + id + "\n" +
            "\t\tProject Title   = " + title + "\n" +
            "\t\tProject Domain  = " + domain + "\n" +
            "\n\t\tProject Description :\n\t\t\t" + description;
    } 

    /**
     * Overrides the equals() method to check object equality
     * @param project of Project datatype
     * @return boolean value true if equals else false.
     */
   public boolean equals(Project project) {
        if( this.id == project.getId()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }    
} 
