package com.ideas2it.employeeProjectManagement.project.service;


import java.util.List;

import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.model.Project;

/**
 * Interface to define standard operations to be performed on Project table
 * @author  Akshay Prabhu S R
 */
public interface ProjectService { 

    /**
     *<p>
     * Creates and sets the values of project details to Project
     *</p>
     *
     * @param project of Project datatype
     *      Will have the project details to be inserted     
     * @return id of int datatype
     *      Will have the auto_incremented value while project is created.
     *      If creation fails, then return 0.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    int createProject(Project project) throws ApplicationException;

   /**
     *<p>
     * Returns the values of all projects 
     *</p>
     *
     *@return projects of List<Project> datatype
     *      Will contain all the projects available 
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */   
    List<Project> getAllProjects() throws ApplicationException;
    
    /**
     *<p>
     * Removes the Project w.r.t projId, if it has no history of employees
 assigned else, soft deletes the project. 
     *</p>
     *
     * @param projId with String datatype. id of a project to be deleted
     *      Project Id- will be of format "PROJ"*, where * is an integer value
     *      Example: PROJ45.
     * @return boolean value
     *      If deletion is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */ 
    boolean removeProjectById(String projId) throws ApplicationException;
    
    /**
     *<p>
     * Creates and sets the values of project details to Project
     *</p>
     *
     * @param project of Project datatype
     *      Will have the project details to be updated   
     * @return boolean value
     *      If updation is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    boolean modifyProject(Project project) throws ApplicationException;
     
   /**
     *<p>
     * Separates the actual integer value of the project that is stored in the 
 database and returns it.
     *</p>
     *
     * @param projId with String datatype
     *      Project Id- will be of format "PROJ"*, where * is an integer value
     *      Example: PROJ45.
     * @return empNo of int datatype
     *      Will have the auto_incremented value while project is created.
     */
    int degenerateProjId(String projId);
    
    /**
     *<p>
     * Validates the given project using validation util and sets the invalid
 fields and addrId as null, if any of the fields are invalid.
     *</p>
     *
     *@param project of Project datatype
     *      Whose values are to be validated
     *@return project of Project datatype
     *      Whose values are validated and set to null if invalid.
     */
    Project validateProject(Project project);
    
    /**
     *<p>
     * Returns the values of project w.r.t id 
     *</p>
     *
     *@param id of int datatype 
     *      Of the project to be retrieved. 
     *@return project of Project datatype
     *      Will contain the project with the given id 
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */   
    Project getProjectById(String projId) throws ApplicationException;
    
    /**
     *<p>
     * Assigns an employee to a project.
     *</p>
     *
     * @param employee with Employee
     *      Which has to be assigned to a project.
     * @param project of Project datatype 
     *      Which has to be assigned to an employee.
     * @return boolean value
     *      If updation is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */ 
    boolean assignEmpToProj(Employee employee, Project project) 
        throws ApplicationException;
        
    /**
     *<p>
     * Unassigns an employee from a project.
     *</p>
     *
     * @param employee with Employee
     *      Which has to be Unassigned from a project.
     * @param project of Project datatype 
     *      Which has to be Unassigned from an employee.
     * @return boolean value
     *      If updation is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */ 
    boolean unassignEmpFromProj(Employee employee, Project project) 
        throws ApplicationException;
}
