package com.ideas2it.employeeProjectManagement.project.service.impl;

import java.lang.Boolean;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.project.model.Project;
import com.ideas2it.employeeProjectManagement.project.dao.ProjectDao;
import com.ideas2it.employeeProjectManagement.project.dao.impl.ProjectDaoImpl;
import com.ideas2it.employeeProjectManagement.project.service.ProjectService;
import com.ideas2it.employeeProjectManagement.util.ValidationUtil;

/**
 * Class to perform business logics and redirect flow to Dao layer to perform
operations on project.
 *
 * @author  Akshay Prabhu S R
**/
public class ProjectServiceImpl implements ProjectService { 
    private ProjectDao projectDao = new ProjectDaoImpl();
      
    /**
     * @see com.ideas2it.employeeProjectManagement.project.service.
     *     ProjectService #Method createProject(String,String,String)
     */
    public int createProject(Project project) throws ApplicationException {     
        return projectDao.insertProject(project);
    }                         

    /**
     * @see com.ideas2it.employeeProjectManagement.project.service.
     *     ProjectService #Method getAllProjects()
     */    
    public List<Project> getAllProjects() throws ApplicationException {
       List<Project> projects = new ArrayList<>();   
        for (Project project : projectDao.retrieveAllProjects()) {
            System.out.println(project);
            projects.add(project);
        }        
        return projects;
    }     
    
    /**
     * @see com.ideas2it.employeeProjectManagement.project.service.
     *     ProjectService #Method removeProject(String)
     */  
    public boolean removeProjectById(String projId) throws ApplicationException {
        return projectDao.deleteProject(getProjectById(projId)); 
    }    
    
    /**
     * @see com.ideas2it.employeeProjectManagement.project.service.
     *     ProjectService #Method modifyProject(String,String,String,String) 
     */
    public boolean modifyProject(Project project) throws ApplicationException {  
        return projectDao.updateProject(project);     
    }
    
    /**
     * @see com.ideas2it.employeeProjectManagement.project.service.
     *     ProjectService #Method degenerateProjId(String)
     */
    public int degenerateProjId(String projId) {
        return Integer.parseInt(projId.substring(projId.indexOf("J")+1));
    }
    
    /**
     * @see com.ideas2it.employeeProjectManagement.project.service.
     *     ProjectService #Method getProjectbyId()
     */    
    public Project getProjectById(String projId) throws ApplicationException {
        return projectDao.retrieveProjById(degenerateProjId(projId));
    }      
    
    /**
     * @see com.ideas2it.employeeProjectManagement.project.service
     *    .ProjectService #Method assignEmpToProj(Employee, Project)
     */   
    public boolean assignEmpToProj(Employee employee, Project project) 
            throws ApplicationException{ 
        project.addEmployee(employee);
        return projectDao.updateProject(project);    
    }  
    
    /**
     * @see com.ideas2it.employeeProjectManagement.project.service
     *    .ProjectService #Method unassignEmpFromProj(Employee, Project)
     */   
    public boolean unassignEmpFromProj(Employee employee, Project project) 
            throws ApplicationException{
        List<Employee> employees = project.getEmployees();
        for (Employee employeeTemp : employees) {
            if (employee.equals(employeeTemp)) { 
                employeeTemp.setProjects(null);
                employees.remove(employeeTemp);
                project.setEmployees(employees);
                break;
            }
        } 
        return projectDao.updateProject(project);    
    } 
    
    /**
     * @see com.ideas2it.employeeProjectManagement.project.service.
     *     ProjectService #Method validateProject(Project)
     */
    public Project validateProject(Project project) {
       if (!ValidationUtil.validateLine(project.getTitle())) {
            project.setTitle(null);
            project.setId(-1);
        }      
        if (!ValidationUtil.validateAlphaLine(project.getDomain())) {
            project.setDomain(null);
            project.setId(-1);
        }     
        if (!ValidationUtil.validateLine(project.getDescription())) {
           project.setDescription(null);
           project.setId(-1);
        }
        return project;
    }
}   
