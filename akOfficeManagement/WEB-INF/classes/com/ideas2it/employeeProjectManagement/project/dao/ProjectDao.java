package com.ideas2it.employeeProjectManagement.project.dao;

import java.util.List;

import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.model.Project;

/**
 * Interface to define standard operations to be performed on Project table
 * @author  Akshay Prabhu S R
 */
public interface ProjectDao {

    /**
     *<p>
     * Inserts the values of project in the database
     *</p>
     *
     * @param project of Project datatype
     *      Will have the details to be inserted
     * @return project of Project datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting a project into the datatype if inserted 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    int insertProject(Project project) throws ApplicationException;

    /**
     *<p>
     * Soft deletes a project from the database by setting isDeleted as "Yes"
     *</p>
     *
     * @param id of int datatype
     *      Will have the auto_generated key for the project to be deleted
     * @return boolean value
     *      If deletion is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */    
    boolean deleteProject(Project project) throws ApplicationException;
     
    /**
     *<p>
     * retrieves an project from the database w.r.t projNo
     *</p>
     *
     * @param id of int datatype
     *      Will have the auto_generated key for the project to be retrieved
     * @return project of Project datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting a project into the database if retrieved 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */     
    Project retrieveProjById(int id) throws ApplicationException;

    /**
     *<p>
     * Retrieves the values of all projects 
     *</p>
     *
     *@return projects of List<Project> datatype
     *      Will contain all the projects available 
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */  
    List<Project> retrieveAllProjects() throws ApplicationException; 
    
     /**
     *<p>
     * Updates the values of project in the database
     *</p>
     *
     * @param project of Project datatype
     *      Will have the details to be updated
     * @return project of Project datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting a project into the database if inserted 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */    
    boolean updateProject(Project project) throws ApplicationException;
}
