package com.ideas2it.employeeProjectManagement.project.dao.impl;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.cfg.Configuration;
import org.hibernate.Criteria;
import org.hibernate.HibernateException; 
import org.hibernate.criterion.Restrictions;
import org.hibernate.Session; 
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ideas2it.employeeProjectManagement.client.dao.ClientDao;
import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.client.service.ClientService;
import com.ideas2it.employeeProjectManagement.client.service.impl.ClientServiceImpl;
import com.ideas2it.employeeProjectManagement.common.Constants;
import com.ideas2it.employeeProjectManagement.connection.ConnectionFactory;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.dao.ProjectDao;
import com.ideas2it.employeeProjectManagement.project.model.Project;
import com.ideas2it.employeeProjectManagement.project.service.ProjectService;
import com.ideas2it.employeeProjectManagement.project.service.impl.ProjectServiceImpl;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import hibernate.HibernateSessionFactory;
import logger.LogManager;

/**
 * Class to implement ProjectDao and perform database operations for Project
from Projects table.
 * 
 * @author  Akshay Prabhu S R
 */
public class ProjectDaoImpl implements ProjectDao {
    private HibernateSessionFactory hibernateSessionFactory = 
        new HibernateSessionFactory();
     private SessionFactory sessionFactory;

    /**
     * @see com.ideas2it.projectProjectManagement.project.Dao.
     *     ProjectDao #Method insertProject(Project)
     */
    public int insertProject(Project project) 
            throws ApplicationException {
        Session session = null;
        try {    
            sessionFactory =  hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            int id = (Integer) session.save(project);
            tx.commit();
            return id;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_PROJ_CR + project, e);
            throw new ApplicationException(Constants.OP_FAILED);
        }  
        finally {            
            session.close(); 
        }                	
    }
    
    /**
     * @see com.ideas2it.employeeProjectManagement.project.Dao.
     *     ProjectDao #Method deleteProject(id)
     */
    public boolean deleteProject(Project project) throws ApplicationException {
        Session session = null;
        try {
            sessionFactory =  hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            project.setIsDeleted(Boolean.TRUE);
            session.update(project);
            tx.commit();
            return Boolean.TRUE;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_PROJ_DEL + project.getId(), e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }     
        finally {
            session.close(); 
        }
    }   

    /**
     * @see com.ideas2it.employeeProjectManagement.project.Dao.
     *     ProjectDao #Method retrieveProject(int)
     */
    public Project retrieveProjById(int id) throws ApplicationException{
        Session session = null;
        try {
            sessionFactory =  hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Project.class)
                .add(Restrictions.eq("id", id));
            Project project = (Project) criteria.uniqueResult();
            tx.commit();
            return project;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_PROJ_RET +  id, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }
        finally {
            session.close(); 
        }        
    }

    /**
     * @see com.ideas2it.employeeProjectManagement.project.Dao.
     *     ProjectDao #Method retrieveAllProjects()
     */
    public List<Project> retrieveAllProjects() throws ApplicationException {
        Session session = null;
        try {
            sessionFactory =  hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Project.class);
            criteria.add(Restrictions.eq("isDeleted", Boolean.FALSE));
            List<Project> projects = criteria.list(); 
            tx.commit();
            return projects;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_PROJS_RET, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }
        finally {
           session.close(); 
        }
    }    
 
    /**
     * @see com.ideas2it.employeeProjectManagement.project.Dao.
     *     ProjectDao #Method updateProject(Project) 
     */
    public boolean updateProject(Project project) 
            throws ApplicationException {
        Session session = null;
        try {
            sessionFactory =  hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            session.update(project);
            tx.commit();
            return Boolean.TRUE;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_PROJ_UPD + project, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }  
        finally {
            session.close(); 
        }
    }
}  
