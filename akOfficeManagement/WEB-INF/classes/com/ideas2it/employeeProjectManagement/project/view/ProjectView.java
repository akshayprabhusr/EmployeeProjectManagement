package com.ideas2it.employeeProjectManagement.project.view;

import java.util.List;
import java.util.Scanner;

import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.project.model.Project;

/**
 * <p>
 * Class to interact with user and redirect flow to controller.
 * </p>
 * @author  Akshay Prabhu S R
**/
public class ProjectView {
    private Scanner scanner = new Scanner(System.in);
    
    /**
     * <p>
     * Gets project details from user to create/update an project.
     * </p>
     *
     * @return project of Project datatype to create/update.
     */
    public Project readProject(Project project) { 
        System.out.println("Enter Project Title: "); 
        project.setTitle(scanner.nextLine());  
        System.out.println("Enter Project Domain : ");
        project.setDomain(scanner.nextLine());   
        System.out.println("Enter Project Description : ");
        project.setDescription(scanner.nextLine());
        return project;         
    }  
    
    /**
     * <p>
     * Displays a list of projects to the user.
     * </p>
     */
    public void displayProjects(List<Project> projects) {
        for (Project project : projects) {
            System.out.println(project.toString());
            System.out.println(project.getClient().toString());
            for (Employee employee : project.getEmployees()) {
                System.out.println(employee.toString());
            }
        }
    }     
    
    /**
     * <p>
     * Gets Project Id from the user.
     * </p>
     *
     * @return Id of project given by user in String datatype 
     */
    public String readProjId() { 
        System.out.println("Enter Project Id :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Project Title from the user, when validation fails.
     * </p>
     *
     * @return name of project given by user in String datatype 
     */
    public String readTitle() { 
        System.out.println("Enter a valid Project Title :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Project Domain from the user.
     * </p>
     *
     * @return Domain of project given by user in String datatype 
     */
    public String readDomain() { 
        System.out.println("Enter a valid Project Domain :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Project Description from the user.
     * </p>
     *
     * @return Description of project given by user in String datatype 
     */
    public String readDescription() { 
        System.out.println("Enter a valid Project Description :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Shows a message to the user.
     * </p>
     *
     * @param msg of String datatype
     *      To be displayed to the user
     */
    public void showMsg(String msg) { 
        System.out.println(msg); 
    }  
}
    
