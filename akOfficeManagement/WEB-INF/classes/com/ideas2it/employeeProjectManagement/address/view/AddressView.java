package com.ideas2it.employeeProjectManagement.address.view;

import java.util.List;
import java.util.Scanner;

import com.ideas2it.employeeProjectManagement.address.model.Address;

/**
 * <p>
 * Class to interact with user and redirect flow to controller.
 * </p>
 * @author  Akshay Prabhu S R
**/
public class AddressView {
    private Scanner scanner = new Scanner(System.in);
    
    /**
     * <p>
     * Gets address details from user to create/update an address.
     * </p>
     *
     * @return address of Address datatype to create/update.
     */
    public Address readAddress(Address address) { 
        System.out.println("Enter DoorNo: "); 
        address.setDoorNo(scanner.nextLine());  
        System.out.println("Enter Street : ");
        address.setStreet(scanner.nextLine());   
        System.out.println("Enter Area : ");
        address.setArea(scanner.nextLine());
        System.out.println("Enter District: "); 
        address.setDistrict(scanner.nextLine());  
        System.out.println("Enter PinCode : ");
        address.setPinCode(scanner.nextLine());   
        System.out.println("Enter State : ");
        address.setState(scanner.nextLine());
        return address;         
    }  
    
    /**
     * <p>
     * Gets Address Id from the user.
     * </p>
     *
     * @return Id of address given by user in String datatype 
     */
    public String readAddrId() { 
        System.out.println("Enter Address Id :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Holder Id from the user.
     * </p>
     *
     * @return Id of address holder given by user in String datatype 
     */
    public String readHldrId() { 
        System.out.println("Enter Holder Id :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets DoorNo from the user, when validation fails.
     * </p>
     *
     * @return name of address given by user in String datatype 
     */
    public String readDoorNo() { 
        System.out.println("Enter a valid DoorNo :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Street from the user.
     * </p>
     *
     * @return Street of address given by user in String datatype 
     */
    public String readStreet() { 
        System.out.println("Enter a valid Street :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Area from the user.
     * </p>
     *
     * @return Area of address given by user in String datatype 
     */
    public String readArea() { 
        System.out.println("Enter a valid Area :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets District from the user, when validation fails.
     * </p>
     *
     * @return name of address given by user in String datatype 
     */
    public String readDistrict() { 
        System.out.println("Enter a valid District :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets PinCode from the user.
     * </p>
     *
     * @return PinCode of address given by user in String datatype 
     */
    public String readPinCode() { 
        System.out.println("Enter a valid PinCode :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets State from the user.
     * </p>
     *
     * @return State of address given by user in String datatype 
     */
    public String readState() { 
        System.out.println("Enter a valid State :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Shows a message to the user.
     * </p>
     *
     * @param msg of String datatype
     *      To be displayed to the user
     */
    public void showMsg(String msg) { 
        System.out.println(msg); 
    }  
}
    
