package com.ideas2it.employeeProjectManagement.address.model;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.client.model.Client;

/*
 * Class to maintain Address details
 * Contains getters and setters to get and set Address details
 * @author Akshay Prabhu S R 
 */
public class Address {    
    private boolean isDeleted = Boolean.FALSE ;
    private int id;    
    private String doorNo;
    private String street;
    private String area;
    private String district;
    private String pinCode;
    private String state;
    private String addrId;
    private Employee employee;
    private Client client;

    public Address(String doorNo,String street,String area,String district,
            String pinCode, String state) {
        this.doorNo = doorNo;
        this.street = street; 
        this.area = area;
        this.district = district;
        this.pinCode = pinCode; 
        this.state = state;
    }  
     
    public Address() {
    }
    
    /**
     * Getters and setters
     */
    public boolean getIsDeleted() {
        return this.isDeleted;
    }
 
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    } 
    
    public int getId() {
        return this.id;
    }
 
    public void setId(int id) {
        this.id = id;
    } 
    
    public String getAddrId() {
        return this.addrId;
    }
 
    public void setAddrId(String addrId) {
        this.addrId = addrId;
    }
    
    public String getDoorNo() {
        return this.doorNo;
    }
 
    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }    
    
    public String getStreet() {
        return this.street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
    
    public String getArea() {
        return this.area;
    }

    public void setArea(String area) {
        this.area = area;
    }  
    
    public String getDistrict() {
        return this.district;
    }
 
    public void setDistrict(String district) {
        this.district = district;
    }    
    
    public String getPinCode() {
        return this.pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
    
    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }  


    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }     
    
     public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }    
    
    /**
     * Overrides the toString() method and prints the project details
     */
    public String toString() {
        return "\n\t\tAddress :AD" + id + "\n\t\t" + doorNo + "," + street +
               ",\n\t\t" + area + ",\n\t\t" + district + "-" + pinCode + 
               ",\n\t\t" + state + ".";
    }   
    
    /**
     * Overrides the equals() method to check object equality
     * @param address of Address datatype
     * @return boolean value true if equals else false.
     */
   public boolean equals(Address address) {
        if( this.id == address.getId()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }     
} 
