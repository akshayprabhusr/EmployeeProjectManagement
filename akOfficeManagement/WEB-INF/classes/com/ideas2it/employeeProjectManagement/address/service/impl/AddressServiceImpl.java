package com.ideas2it.employeeProjectManagement.address.service.impl;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import com.ideas2it.employeeProjectManagement.address.dao.AddressDao;
import com.ideas2it.employeeProjectManagement.address.dao.impl.AddressDaoImpl;
import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.address.service.AddressService;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.employee.service.EmployeeService;
import com.ideas2it.employeeProjectManagement.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.client.service.ClientService;
import com.ideas2it.employeeProjectManagement.client.service.impl.ClientServiceImpl;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.util.ValidationUtil;

/**
 * Class to perform business logics and redirect flow to Dao layer to perform
operations on Address.
 *
 * @author  Akshay Prabhu S R
**/
public class AddressServiceImpl implements AddressService {     
    private AddressDao addressDao = new AddressDaoImpl();
              
    /**
     * @see com.ideas2it.addressProjectManagement.address.service.
     *     AddressService #Method addAddress(String,String,String,
     *                                        String,String,String)
     */
    public int createAddress(Address address) throws ApplicationException {   
        return addressDao.insertAddress(address);
    } 
      
    /**
     * @see com.ideas2it.addressProjectManagement.address.service.
     *     AddressService #Method deleteAddress(String) 
     */ 
    public boolean removeAddressById(String addrId) 
            throws ApplicationException {
        Address address = getAddressById(degenerateAddrId(addrId));
        if (address != null) {
            return addressDao.deleteAddress(address);
        } else {
            return Boolean.FALSE;
        }      
    } 
       
    /**
     * @see com.ideas2it.addressProjectManagement.address.service.
     *     AddressService #Method modifyAddress(String,String,String,String)
     */
    public boolean modifyAddress(Address address) 
            throws ApplicationException {       
       	address.setId(degenerateAddrId(address.getAddrId()));
        return addressDao.updateAddress(address);     
    }     
     
    /**
     * @see com.ideas2it.addressProjectManagement.address.service.
     *     AddressService #Method checkAddressById(String)
     */
    public int degenerateAddrId(String addrId) {
        return Integer.parseInt(addrId.substring(addrId.indexOf("D")+1));
    }  

    /**
     * @see com.ideas2it.employeeProjectManagement.address.service.
     *     AddressService #Method getAddressbyId()
     */    
    public Address getAddressById(int id) throws ApplicationException {
        return addressDao.retrieveAddrById(id);
    }      
    
    /**
     * @see com.ideas2it.employeeProjectManagement.address.service.
     *     AddressService #Method validateAddress(Address)
     */
    public Address validateAddress(Address address) {
       if (!ValidationUtil.validateLine(address.getDoorNo())) {
            address.setDoorNo(null);
            address.setId(-1);
        }      
        if (!ValidationUtil.validateLine(address.getStreet())) {
            address.setStreet(null);
            address.setId(-1);
        }     
        if (!ValidationUtil.validateLine(address.getArea())) {
           address.setArea(null);
           address.setId(-1);
        }
        if (!ValidationUtil.validateAlphaLine(address.getDistrict())) {
            address.setDistrict(null);
            address.setId(-1);
        }      
        if (!ValidationUtil.validateAlphaLine(address.getPinCode())) {
            address.setPinCode(null);
            address.setId(-1);
        }     
        if (!ValidationUtil.validateAlphaLine(address.getState())) {
           address.setState(null);
           address.setId(-1);
        }
        return address;
    }
}    
