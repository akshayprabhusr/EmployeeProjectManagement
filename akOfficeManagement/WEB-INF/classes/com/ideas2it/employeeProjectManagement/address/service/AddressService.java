package com.ideas2it.employeeProjectManagement.address.service;

import java.util.List;

import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;

/**
 * Interface to define standard operations to be performed on Address
 * @author  Akshay Prabhu S R
 */
public interface AddressService {       
              
    /**
     *<p>
     * Creates and sets the values of employee details to Employee
     *</p>
     *
     * @param address of Address datatype
     *      Will have the address details to be inserted      
     * @return addrNo of int datatype
     *      Will have the auto_incremented value while address is created.
     *      If insertion fails, then return 0.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    int createAddress(Address address) throws ApplicationException;                         

    /**
     *<p>
     * Removes the Address w.r.t addrId, if it has no history of employees
 assigned else, soft deletes the project. 
     *</p>
     *
     * @param projId with String datatype. id of a project to be deleted
     *      Project Id- will be of format "PROJ"*, where * is an integer value
     *      Example: PROJ45.
     * @return boolean value
     *      If deletion is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */  
          
    boolean removeAddressById(String addrId) throws ApplicationException;    
    
   /**
     *<p>
     * Creates and sets the values of employee details to Employee
     *</p>
     *
     * @param address of Address datatype
     *      Will have the address details to be updated     
     * @return boolean value
     *      If updation is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    boolean modifyAddress(Address address) throws ApplicationException;
     
    /**
     *<p>
     * Separates the actual integer value of the address that is stored in the 
 database and returns it.
     *</p>
     *
     * @param addrId with String datatype
     *      AddrId- will be of format "AD"*, where * is an integer value
     *      Example: PROJ45.
     * @return addrNo of int datatype
     *      Will have the auto_incremented value while address is created.    
     */
    int degenerateAddrId(String addrId) throws ApplicationException;  

    /**
     *<p>
     * Validates the given address using validation util and sets the invalid
 fields and addrId as null, if any of the fields are invalid.
     *</p>
     *
     *@param address of Address datatype
     *      Whose values are to be validated
     *@return address of Address datatype
     *      Whose values are validated and set to null if invalid.
     */
    Address validateAddress(Address address);
    
    /**
     *<p>
     * Returns the values of address w.r.t id 
     *</p>
     *
     *@param id of int datatype 
     *      Of the address to be retrieved. 
     *@return address of Address datatype
     *      Will contain the address with the given id 
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */   
    Address getAddressById(int id) throws ApplicationException;
}
