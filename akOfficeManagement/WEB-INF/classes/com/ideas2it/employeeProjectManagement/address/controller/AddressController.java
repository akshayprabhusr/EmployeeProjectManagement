package com.ideas2it.employeeProjectManagement.address.controller;

import java.lang.Boolean;
import java.util.Scanner;

import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.address.view.AddressView;
import com.ideas2it.employeeProjectManagement.address.service.AddressService;
import com.ideas2it.employeeProjectManagement.address.service.impl.AddressServiceImpl;
import com.ideas2it.employeeProjectManagement.common.Constants;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.employee.service.EmployeeService;
import com.ideas2it.employeeProjectManagement.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.client.service.ClientService;
import com.ideas2it.employeeProjectManagement.client.service.impl.ClientServiceImpl;
import com.ideas2it.employeeProjectManagement.util.ValidationUtil;


/**
 * Class to interact with user and redirect flow to service layer to perform
operations on Address.
 *
 * @author  Akshay Prabhu S R
**/
public class AddressController {
    private AddressService addressService = new AddressServiceImpl();
    private ClientService clientService = new ClientServiceImpl();
    private EmployeeService employeeService = new EmployeeServiceImpl();
    private AddressView addressView = new AddressView();
    Scanner scanner = new Scanner(System.in);
    
   }
