package com.ideas2it.employeeProjectManagement.address.dao;

import java.util.List;

import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;

/**
 * Interface to define standard operations to be performed on Addresss table
 *
 * @author  Akshay Prabhu S R
 */
public interface AddressDao {
    
    /**
     *<p>
     * Inserts the values of Address in the database
     *</p>
     *
     * @param address of Address datatype
     *      Will have the details to be inserted
     * @return address of Address datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting an address into the datatype if inserted 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    int insertAddress(Address address) throws ApplicationException;

    /**
     *<p>
     * Retrieves an address from the database w.r.t addrId
     *</p>
     *
     * @param id of int datatype
     *      Will have the auto_generated key for the address to be retrieved
     * @return address of Address datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting an address into the database if retrieved 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */  
    Address retrieveAddrById(int id) throws ApplicationException;    
    
    /**
     *<p>
     * Soft deletes an address from the database by setting isDeleted as "Yes"
     *</p>
     *
     * @param addrId of int datatype
     *      Will have the auto_generated key for the address to be deleted
     * @return boolean value
     *      If deletion is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    boolean deleteAddress(Address address) throws ApplicationException;    

    /**
     *<p>
     * Updates the values of address in the database
     *</p>
     *
     * @param address of Address datatype
     *      Will have the details to be updated
     * @return boolean value
     *      If deletion is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */         
    boolean updateAddress(Address address) throws ApplicationException;       
}
