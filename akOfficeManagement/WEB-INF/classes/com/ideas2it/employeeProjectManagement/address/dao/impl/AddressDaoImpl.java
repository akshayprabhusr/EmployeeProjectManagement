package com.ideas2it.employeeProjectManagement.address.dao.impl;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException; 
import org.hibernate.criterion.Restrictions;
import org.hibernate.Session; 
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ideas2it.employeeProjectManagement.address.dao.AddressDao;
import com.ideas2it.employeeProjectManagement.address.service.AddressService;
import com.ideas2it.employeeProjectManagement.address.service.impl.AddressServiceImpl;
import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.common.Constants;
import com.ideas2it.employeeProjectManagement.employee.service.EmployeeService;
import com.ideas2it.employeeProjectManagement.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.client.service.ClientService;
import com.ideas2it.employeeProjectManagement.client.service.impl.ClientServiceImpl;
import hibernate.HibernateSessionFactory;
import logger.LogManager;


/**
 * Class to implement AddressDao and perform database operations for Address
from Address table.
 * 
 * @author  Akshay Prabhu S R
**/
public class AddressDaoImpl implements AddressDao {
    private HibernateSessionFactory hibernateSessionFactory = 
        new HibernateSessionFactory();
     private SessionFactory sessionFactory; 

    /**
     * @see com.ideas2it.employeeProjectManagement.address.Dao.
     *     AddressDao #Method insertAddress(Address)
     */
    public int insertAddress(Address address) throws ApplicationException {   
            Session session = null;
        try {    
            sessionFactory =  hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();  
            int id = (Integer) session.save(address);
            tx.commit();
            return id;
        } catch (Exception e) {e.printStackTrace();
            LogManager.handleError(Constants.EX_ADDR_CR + address, e);
            throw new ApplicationException(Constants.OP_FAILED);
        }  
        finally {            
          //   session.close();
        }                	
    }
    
    /**
     * @see com.ideas2it.employeeProjectManagement.address.Dao.
     *     AddressDao #Method retrieveAddress(int)
     */
    public Address retrieveAddrById(int id) throws ApplicationException{
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Address.class)
                .add(Restrictions.eq("id", id));
            Address address = (Address) criteria.uniqueResult();
            tx.commit();
            return address;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_ADDR_RET +  id, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }
        finally {
           // session.close(); 
        }        
    }
    
    /**
     * @see com.ideas2it.employeeProjectManagement.address.Dao.
     *     AddressDao #Method deleteAddress(id)
     */
    public boolean deleteAddress(Address address) throws ApplicationException {
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            address.setIsDeleted(Boolean.TRUE);
            session.update(address);
            tx.commit();
            return Boolean.TRUE;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_ADDR_DEL + address.getId(), e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }     
        finally {
          //  session.close(); 
        }
    }  
    
    /**
     * @see com.ideas2it.employeeProjectManagement.address.Dao.
     *     AddressDao #Method updateAddress(Address,int) 
     */
    public boolean updateAddress(Address address) throws ApplicationException {
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            session.update(address);
            tx.commit();
            return Boolean.TRUE;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_ADDR_UPD + address, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }  
        finally {
          //  session.close(); 
        }
    }
}
