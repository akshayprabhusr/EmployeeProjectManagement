package com.ideas2it.employeeProjectManagement.client.service.impl;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import com.ideas2it.employeeProjectManagement.address.service.AddressService;
import com.ideas2it.employeeProjectManagement.address.service.impl.AddressServiceImpl;
import com.ideas2it.employeeProjectManagement.client.dao.ClientDao;
import com.ideas2it.employeeProjectManagement.client.dao.impl.ClientDaoImpl;
import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.client.service.ClientService;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.model.Project;
import com.ideas2it.employeeProjectManagement.project.service.ProjectService;
import com.ideas2it.employeeProjectManagement.project.service.impl.ProjectServiceImpl;
import com.ideas2it.employeeProjectManagement.util.ValidationUtil;

/**
 * Class to perform business logics and redirect flow to Dao layer to perform
operations on Client.
 *
 * @author  Akshay Prabhu S R
**/
public class ClientServiceImpl implements ClientService {     
    private ClientDao clientDao = new ClientDaoImpl(); 
    private ProjectService projectService = new ProjectServiceImpl();
    private AddressService addressService = new AddressServiceImpl(); 
    
              
    /**
     * @see com.ideas2it.clientProjectManagement.client.service.
     *     ClientService #Method createClient(String,String,String)
     */
    public int createClient(Client client) throws ApplicationException {  
        return clientDao.insertClient(client);
    }                         

    /**
     * @see com.ideas2it.clientProjectManagement.client.service.
     *     ClientService #Method getAllClients()
     */    
    public List<Client> getAllClients() throws ApplicationException {
        List<Client> clients = new ArrayList<>(); 
        return clientDao.retrieveAllClients();
    }
    
     /**
     * @see com.ideas2it.employeeProjectManagement.client.service.
     *     ClientService #Method removeClient(String) 
     */ 
    public boolean removeClientById(String clientId) throws ApplicationException {
        Client client = getClientById(clientId);
        if (client != null) {
            return clientDao.deleteClient(client);
        } else {
            return Boolean.FALSE;
        } 
    }     
    
    /**
     * @see com.ideas2it.clientProjectManagement.client.service.
     *     ClientService #Method modifyClient(String,String,String,String)
     */
    public boolean modifyClient(Client client) throws ApplicationException {  
        return clientDao.updateClient(client);     
    }
     
    /**
     * @see com.ideas2it.clientProjectManagement.client.service.
     *     ClientService #Method checkClientById(String)
     */
    public int degenerateClientId(String clientId) {
        return Integer.parseInt(clientId.substring(clientId.indexOf("T")+1));
    }  

    /**
     * @see com.ideas2it.employeeProjectManagement.client.service.
     *     ClientService #Method getClientbyId()
     */    
    public Client getClientById(String clientId) throws ApplicationException {
        return clientDao.retrieveClientById(degenerateClientId(clientId));
    }      
    
    /**
     * @see com.ideas2it.employeeProjectManagement.client.service.
     *     ClientService #Method validateClient(Client)
     */
    public Client validateClient(Client client) {
       if (!ValidationUtil.validateName(client.getName())) {
            client.setName(null);
            client.setId(-1);
        }      
        if (!ValidationUtil.validateEmailId(client.getEmailId())) {
            client.setEmailId(null);
            client.setId(-1);
        }     
        if (!ValidationUtil.validateLine(client.getOrg())) {
           client.setOrg(null);
           client.setId(-1);
        }
        return client;
    }
}    
