package com.ideas2it.employeeProjectManagement.client.service;

import java.util.List;

import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.model.Project;

/**
 * Interface to define standard operations to be performed on Client
 * @author  Akshay Prabhu S R
 */
public interface ClientService {       
              
    /**
     *<p>
     * Creates and sets the values of client details to Client
     *</p>
     *
     *@param client of Client datatype
     *      Will have the client details to be inserted     
     * @return clientNo of int datatype
     *      Will have the auto_incremented value while client is created.
     *      If creation fails, then return 0.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    int createClient(Client client) throws ApplicationException;               

    /**
     *<p>
     * Returns the values of all clients
     *</p>
     *
     *@return clients of List<Client> datatype
     *      Will contain all the clients available 
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */    
    List<Client> getAllClients() throws ApplicationException;
    
     /**
     *<p>
     * Removes the Client w.r.t clientId, if it has no history of clients
 assigned else, soft deletes the client. 
     *</p>
     *
     * @param clientId with String datatype. id of a client to be deleted
     *      Client Id- will be of format "CT"*, where * is an integer value
     *      Example: CT45.
     * @return boolean value
     *      If deletion is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */ 
    boolean removeClientById(String clientId) throws ApplicationException;    
    
    /**
     *<p>
     * Creates and sets the values of client details to Client.
     *</p>
     *
     * @param client of Client datatype
     *      Will have the client details to be inserted     
     *@return boolean value
     *      If updated is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    boolean modifyClient(Client client) throws ApplicationException;   

   /**
     *<p>
     * Separates the actual integer value of the project that is stored in the 
 database and returns it.
     *</p>
     *
     * @param clientId with String datatype
     *      Client Id- will be of format "CT"*, where * is an integer value
     *      Example: CT45.
     * @return clientNo of int datatype
     *      Will have the auto_incremented value while client is created.     
     */
    int degenerateClientId(String clientId);  

    /**
     *<p>
     * Validates the given client using validation util and sets the invalid
 fields and clientId as null, if any of the fields are invalid.
     *</p>
     *
     *@param client of Client datatype
     *      Whose values are to be validated
     *@return client of Client datatype
     *      Whose values are validated and set to null if invalid.
     */
    Client validateClient(Client client);
    
    /**
     *<p>
     * Returns the values of client w.r.t id 
     *</p>
     *
     *@param id of int datatype 
     *      Of the client to be retrieved. 
     *@return client of Client datatype
     *      Will contain the client with the given id 
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */   
    Client getClientById(String clientId) throws ApplicationException;
}
