package com.ideas2it.employeeProjectManagement.client.model;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.List;

import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.project.model.Project;

/*
 * Class to maintain Client details
 * Contains getters and setters to get and set Client details
 * @author Akshay Prabhu S R 
 */
public class Client {    
    private boolean isDeleted = Boolean.FALSE ;
    private int id;    
    private String name;
    private String org;
    private String emailId;
    private String clientId; 
    private List<Project> projects = new ArrayList<>();
    private List<Address> addresses = new ArrayList<>();

    public Client(String name,String org,String emailId) {
        this.name = name;
        this.org = org; 
        this.emailId = emailId;
    }    
    
    public Client() {
    }
    
    /**
     * Getters and setters
     */  
    public boolean getIsDeleted() {
        return this.isDeleted;
    }
 
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    } 
      
    public int getId() {
        return this.id;
    }
 
    public void setId(int id) {
        this.id = id;
    } 
    
    public String getClientId() {
        return this.clientId;
    }
 
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    
    public String getName() {
        return this.name;
    }
 
    public void setName(String name) {
        this.name = name;
    }    
    
    public String getOrg() {
        return this.org;
    }

    public void setOrg(String org) {
        this.org = org;
    }
    
    public String getEmailId() {
        return this.emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }  

    public List<Project> getProjects() {
        return this.projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }  
    
    public void addProject(Project project) {
        this.projects.add(project);
    } 
           
    public List<Address> getAddresses() {
        return this.addresses;
    }
    
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }  
    
    public void addAddress(Address address) {
        this.addresses.add(address);
    }
    
    /**
     * Overrides the toString() method and prints the project details
     */
    public String toString() {
        return "\n\t\tClient Id     = CT" + id + "\n" +
               "\t\tClient Name     = " + name + "\n" +
               "\t\tClient Email Id = " + emailId + "\n" +
               "\t\tClient Organization  = " + org + "\n";
    } 
    
    /**
     * Overrides the equals() method to check object equality
     *
     * @param client of Client datatype
     * @return boolean value true if equals else false.
     */
    public boolean equals(Client client) {
        if( this.id == client.getId()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }    
} 
