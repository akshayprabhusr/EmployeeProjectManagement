package com.ideas2it.employeeProjectManagement.client.view;

import java.util.List;
import java.util.Scanner;

import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.project.model.Project;

/**
 * <p>
 * Class to interact with user and redirect flow to controller.
 * </p>
 * @author  Akshay Prabhu S R
**/
public class ClientView {
    private Scanner scanner = new Scanner(System.in);
    
    /**
     * <p>
     * Gets client details from user to create/update an client.
     * </p>
     *
     * @return client of Client datatype to create/update.
     */
    public Client readClient(Client client) { 
        System.out.println("Enter Client Name: "); 
        client.setName(scanner.nextLine());  
        System.out.println("Enter Client Organization : ");
        client.setOrg(scanner.nextLine());   
        System.out.println("Enter Client Email-Id : ");
        client.setEmailId(scanner.nextLine());
        return client;         
    }  
    
    /**
     * <p>
     * Displays a list of clients to the user.
     * </p>
     */
    public void displayClients(List<Client> clients) {
        for (Client client : clients) {
            System.out.println(client.toString());
            for (Address address : client.getAddresses()) {
                System.out.println(address.toString());
            }
            for (Project project : client.getProjects()) {
                System.out.println(project.toString());
            }
        }
    }     
    
    /**
     * <p>
     * Gets Client Id from the user.
     * </p>
     *
     * @return Id of client given by user in String datatype 
     */
    public String readClientId() { 
        System.out.println("Enter Client Id :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Client Name from the user, when validation fails.
     * </p>
     *
     * @return name of client given by user in String datatype 
     */
    public String readName() { 
        System.out.println("Enter a valid Client Name :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Client Mobile Number from the user.
     * </p>
     *
     * @return Mobile Number of client given by user in String datatype 
     */
    public String readOrg() { 
        System.out.println("Enter a valid Organization :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Client Email-Id from the user.
     * </p>
     *
     * @return Email-Id of client given by user in String datatype 
     */
    public String readEmailId() { 
        System.out.println("Enter a valid Email-Id :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Shows a message to the user.
     * </p>
     *
     * @param msg of String datatype
     *      To be displayed to the user
     */
    public void showMsg(String msg) { 
        System.out.println(msg); 
    }  
}
