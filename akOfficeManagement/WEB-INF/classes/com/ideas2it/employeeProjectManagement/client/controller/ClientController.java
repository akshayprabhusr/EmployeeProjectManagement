package com.ideas2it.employeeProjectManagement.client.controller;

import java.io.IOException;
import java.lang.Boolean;
import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.address.service.AddressService;
import com.ideas2it.employeeProjectManagement.address.service.impl.AddressServiceImpl;
import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.client.service.ClientService;
import com.ideas2it.employeeProjectManagement.client.service.impl.ClientServiceImpl;
import com.ideas2it.employeeProjectManagement.common.Constants;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.model.Project;
import com.ideas2it.employeeProjectManagement.util.ValidationUtil;

/**
 * Class to interact with user and redirect flow to service layer to perform
operations on Client.
 *
 * @author  Akshay Prabhu S R
**/   
public class ClientController extends HttpServlet {
    private ClientService clientService = new ClientServiceImpl();
    private AddressService addressService = new AddressServiceImpl();

    public void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        displayClients(request, response);           
    } 
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) throws ServletException, IOException {  
        switch (request.getParameter("action")) {
            case "addClient":
                addClient(request, response);
                break;
            case "displayAll":
                displayClients(request, response);
                break;
            case "display":
                displayClient(request, response);
                break;
            case "delete":
                deleteClient(request, response);
                break;
            case "toUpdate":
                getClientToUpdate(request, response);
                break;
            case "update":
                updateClient(request, response);
                break;
            default :
                break;            
        }    
    }
    
    private void deleteClient(HttpServletRequest request,
            HttpServletResponse response) {
                        System.out.println(request);
        try {
            clientService.removeClientById("CT" + request.getParameter("clientId"));
            request.setAttribute("clients", clientService.getAllClients());
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/Client.jsp");
            rd.forward(request, response);
        } catch (ServletException e) {
            System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }    
    }
    
    private void displayClients(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        try {    
            request.setAttribute("clients", clientService.getAllClients());
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/Client.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }
    
    }
    
    private void addClient(HttpServletRequest request,
            HttpServletResponse response) {
        try {  
            Client client = new Client();
            client.setName(request.getParameter("clientName"));
            client.setEmailId(request.getParameter("clientEmailId"));
            client.setOrg(request.getParameter("org"));
            Address address = new Address();
            address.setDoorNo(request.getParameter("doorNo"));
            address.setStreet(request.getParameter("street"));
            address.setArea(request.getParameter("area"));
            address.setDistrict(request.getParameter("district"));
            address.setPinCode(request.getParameter("pincode"));
            address.setState(request.getParameter("state"));
            client.addAddress(address);
            clientService.createClient(client); 
            request.setAttribute("clients", clientService.getAllClients());
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/Client.jsp");
            rd.forward(request, response);   
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }   
    }
    
    private void getClientToUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        try {
            Client client = clientService.getClientById(request.getParameter("clientId"));
            request.setAttribute("client",client);          
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/UpdateClient.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }       
    }
    
    private void updateClient(HttpServletRequest request,
            HttpServletResponse response) {
        try {
            Client client = clientService.getClientById(request.getParameter("clientId"));
            client.setName(request.getParameter("clientName"));
            client.setEmailId(request.getParameter("clientEmailId"));
            client.setOrg(request.getParameter("org"));
            System.out.println(client);
            clientService.modifyClient(client); 
            request.setAttribute("clients", clientService.getAllClients());
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/Client.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }   
    
    }
    
    private void displayClient(HttpServletRequest request,
            HttpServletResponse response) {
        try {
            Client client = clientService.getClientById(request.getParameter("clientId"));
            request.setAttribute("client", client);      
            RequestDispatcher rd = request.getRequestDispatcher("/jsp/ShowClient.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }   
    }
}
