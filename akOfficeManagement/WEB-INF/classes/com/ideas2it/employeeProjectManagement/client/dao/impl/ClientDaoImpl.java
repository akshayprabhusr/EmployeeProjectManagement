package com.ideas2it.employeeProjectManagement.client.dao.impl;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.cfg.Configuration;
import org.hibernate.Criteria;
import org.hibernate.HibernateException; 
import org.hibernate.criterion.Restrictions;
import org.hibernate.Session; 
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ideas2it.employeeProjectManagement.client.dao.ClientDao;
import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.client.service.ClientService;
import com.ideas2it.employeeProjectManagement.client.service.impl.ClientServiceImpl;
import com.ideas2it.employeeProjectManagement.common.Constants;
import com.ideas2it.employeeProjectManagement.connection.ConnectionFactory;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.service.ProjectService;
import com.ideas2it.employeeProjectManagement.project.service.impl.ProjectServiceImpl;
import com.ideas2it.employeeProjectManagement.project.model.Project;
import hibernate.HibernateSessionFactory;
import logger.LogManager;

/**
 * Class to implement ClientDao and perform database operations for Client
from Clients table.
 * 
 * @author  Akshay Prabhu S R
**/
public class ClientDaoImpl implements ClientDao {
    private HibernateSessionFactory hibernateSessionFactory = 
        new HibernateSessionFactory();
     private SessionFactory sessionFactory; 
    

    /**
     * @see com.ideas2it.employeeProjectManagement.client.Dao.
     *     ClientDao #Method insertClient(Client)
     */
    public int insertClient(Client client) 
            throws ApplicationException {
        Session session = null;
        try {    
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            int id = (Integer) session.save(client);
            tx.commit();
            return id;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_CLIENT_CR + client, e);
            throw new ApplicationException(Constants.OP_FAILED);
        }
        finally {            
            session.close(); 
        }                	
    }
    
    /**
     * @see com.ideas2it.employeeProjectManagement.client.Dao.
     *     ClientDao #Method deleteClient(id)
     */
    public boolean deleteClient(Client client) throws ApplicationException {
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            client.setIsDeleted(Boolean.TRUE);
            Transaction tx = session.beginTransaction();
            session.update(client);
            tx.commit();
            return Boolean.TRUE;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_CLIENT_DEL + client.getId(), e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }     
        finally {
            session.close(); 
        }
    }   

    /**
     * @see com.ideas2it.employeeProjectManagement.client.Dao.
     *     ClientDao #Method retrieveClient(int)
     */
    public Client retrieveClientById(int id) throws ApplicationException{
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Client.class)
                .add(Restrictions.eq("id", id));
            Client client = (Client) criteria.uniqueResult();
            tx.commit();
            return client;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_CLIENT_RET +  id, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }
        finally {
            session.close(); 
        }        
    }

    /**
     * @see com.ideas2it.employeeProjectManagement.client.Dao.
     *     ClientDao #Method retrieveAllClients()
     */
    public List<Client> retrieveAllClients() throws ApplicationException {
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Client.class);
            criteria.add(Restrictions.eq("isDeleted", Boolean.FALSE));
            List<Client> clients = criteria.list(); 
            tx.commit();
            return clients;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_CLIENTS_RET, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }
        finally {
            session.close(); 
        }
    }    
 
    /**
     * @see com.ideas2it.employeeProjectManagement.client.Dao.
     *     ClientDao #Method updateClient(Client) 
     */
    public boolean updateClient(Client client) 
            throws ApplicationException {
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            session.update(client);
            tx.commit();
            return Boolean.TRUE;
        } catch (ApplicationException e) {
            LogManager.handleError(Constants.EX_CLIENT_UPD +
                client, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }  
        finally {
            session.close(); 
        }
    }
}
