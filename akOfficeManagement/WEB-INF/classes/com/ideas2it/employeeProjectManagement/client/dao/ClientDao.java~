package com.ideas2it.employeeProjectManagement.client.dao;

import java.util.List;

import com.ideas2it.employeeProjectManagement.client.model.Client;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.model.Project;

/**
 * Interface to define standard operations to be performed on Clients table
 *
 * @author  Akshay Prabhu S R
 */
public interface ClientDao {
    
    /**
     *<p>
     * Inserts the values of client in the database
     *</p>
     *
     * @param client of Client datatype
     *      Will have the details to be inserted
     * @return client of Client datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting a client into the datatype if inserted 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    int insertClient(Client client) throws ApplicationException;

    /**
     *<p>
     * Soft deletes a client from the database by setting isDeleted as "Yes"
     *</p>
     *
     * @param client of Client datatype
     *      Will have the details to be inserted
     * @return boolean value
     *      If deletion is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */     
    boolean deleteClient(Client client) throws ApplicationException;
    
    /**
     *<p>
     * Retrieves an employee from the database w.r.t empNo
     *</p>
     *
     * @param clientId of int datatype
     *      Will have the auto_generated key for the client to be retrieved
     * @return employee of Client datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting a client into the database. if retrieved 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */  
    Client retrieveClientById(int id) throws ApplicationException;

    /**
     *<p>
     * Retrieves the values of all clients 
     *</p>
     *
     *@return clients of List<Client> datatype
     *      Will contain all the clients available.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed. 
     */  
    List<Client> retrieveAllClients() throws ApplicationException;
 
    /**
     *<p>
     * Updates the values of client in the database
     *</p>
     *
     * @param client of Client datatype
     *      Will have the details to be updated
     * @return client of Client datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting a client into the database if inserted 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */    
    boolean updateClient(Client client) throws ApplicationException;
}
