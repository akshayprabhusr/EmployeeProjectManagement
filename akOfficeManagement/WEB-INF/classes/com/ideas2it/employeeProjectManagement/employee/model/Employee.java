package com.ideas2it.employeeProjectManagement.employee.model;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.project.model.Project;
import com.ideas2it.employeeProjectManagement.util.DateUtil;

/*
 * Class to maintain Employee details
 * Contains getters and setters to get and set Employee details
 * @author Akshay Prabhu S R 
 */
public class Employee {  
    private boolean isDeleted = Boolean.FALSE ; 
    private int id;    
    private String name;
    private String mobileNumber;
    private String emailId;
    private String empId;      
    private String dob;
    private List<Address> addresses = new ArrayList<>();
    private List<Project> projects = new ArrayList<>();  
    
    public Employee(String name,String mobileNumber,String emailId,
            String dob) {
        this.name = name;
        this.mobileNumber = mobileNumber; 
        this.emailId = emailId;
        this.dob = dob;
    }  
      
    public Employee() {
    }
    
    /**
     * Getters and setters
     */
    public boolean getIsDeleted() {
        return this.isDeleted;
    }
 
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    } 
    
    public int getId() {
        return this.id;
    }
 
    public void setId(int id) {
        this.id = id;
    } 
     
    public String getEmpId() {
        return this.empId;
    }
 
    public void setEmpId(String empId) {
        this.empId = empId;
    }
    
    public String getName() {
        return this.name;
    }
 
    public void setName(String name) {
        this.name = name;
    }    
    
    public String getMobileNumber() {
        return this.mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    public String getEmailId() {
        return this.emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    } 
   
    public String getDob() {
        return this.dob;
    }
 
    public void setDob(String dob) {
        this.dob = dob;
    }  
 
    public int getAge(String dob) {
        return this.calculateAge(dob);
    }   
    
    public List<Address> getAddresses() {
        return this.addresses;
    }
    
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }  
    
    public void addAddress(Address address) {
        this.addresses.add(address);
    }
    
    public List<Project> getProjects() {
        return this.projects;
    }
    
    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }  
    
    public void addProject(Project project) {
        this.projects.add(project);
    }
    
    /**
     *<p>
     * Overrides the toString() method to print the employee details in the 
 required format
     *</p>
     */
    public String toString() {
        return "\n\t\tEmployee Id     = I2IT" + id + "\n" +
            "\t\tEmployee Name     = " + name + "\n" +
            "\t\tEmployee Email Id = " + emailId + "\n" +
            "\t\tEmployee Contact Number  = " + mobileNumber + "\n" +
            "\t\tEmployee Date of Birth  = " + dob + "\n";// +
            //"\t\tEmployee Age      = " + getAge(dob); 
               
    } 
    
   /**
     *<p>
     * Overrides equals() method for Employee datatype by comparing the empId
 of the current Employee and the Employee object passed.
     *</p>
     * 
     * @param employee of Employee datatype
     *      employee that has to be checked with current Employee
     * @return boolean value
     *      Boolean.TRUE if both objects have the same empId, else Boolean.FALSE 
     */    
    public boolean equals(Employee employee) {
        if( this.id == employee.getId()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
    
   /**
     *<p>
     * Calculates the age of an employee w.r.t Date Of Birth
     *</p>
     * 
     * @param dateAsString with datatype datatype
     *      Will follow a regular expression to contain a format dd/mm/yyyy
     *      Example: 20/04/1996.
     * @return int datatype
     *      Will return the employee's current age by comparing the values of 
     *      Date Of Birth and the current date.
     * 
     */    
    public int calculateAge(String dateAsString) {
        return DateUtil.calculateDateDiff(dateAsString);
    }    
} 
