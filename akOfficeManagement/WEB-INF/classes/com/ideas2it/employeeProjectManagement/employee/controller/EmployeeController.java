package com.ideas2it.employeeProjectManagement.employee.controller;

import java.io.IOException;
import java.lang.Boolean;
import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ideas2it.employeeProjectManagement.common.Constants;
import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.address.service.AddressService;
import com.ideas2it.employeeProjectManagement.address.service.impl.AddressServiceImpl;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.employee.service.EmployeeService;
import com.ideas2it.employeeProjectManagement.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.model.Project;
import com.ideas2it.employeeProjectManagement.project.service.ProjectService;
import com.ideas2it.employeeProjectManagement.project.service.impl.ProjectServiceImpl;
import com.ideas2it.employeeProjectManagement.util.DateUtil;
import com.ideas2it.employeeProjectManagement.util.ValidationUtil;

/**
 *<p>
 * Class to interact with view and redirect flow to service layer to perform
operations on Employee.
 * </p>
 * @author  Akshay Prabhu S R
**/
public class EmployeeController extends HttpServlet {
    private EmployeeService employeeService = new EmployeeServiceImpl();
    private AddressService addressService = new AddressServiceImpl();
    private ProjectService projectService = new ProjectServiceImpl();

    public void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    } 
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) throws ServletException, IOException {  
        switch (request.getParameter("action")) {
            case "addEmployee":
                addEmployee(request, response);
                break;
            case "displayAll":
                displayEmployees(request, response);
                break;
            case "display":
                displayEmployee(request, response);
                break;
            case "delete":
                deleteEmployee(request, response);
                break;
            case "toUpdate":
                getEmployeeToUpdate(request, response);
                break;
            case "Update":
                updateEmployee(request, response);
                break;
            default :
                break;            
        }    
    }
    
    private void deleteEmployee(HttpServletRequest request,
            HttpServletResponse response) {
                        System.out.println(request);
        try {
            employeeService.removeEmployeeById("I2IT" + 
                request.getParameter("empId"));
            request.setAttribute("employees", employeeService.getAllEmployees());
            RequestDispatcher rd = request.
                getRequestDispatcher("/jsp/Employee.jsp");
            rd.forward(request, response);
        } catch (ServletException e) {
            System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }    
    }
    
    private void displayEmployees(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        try {    
            request.setAttribute("employees", employeeService.getAllEmployees());
            RequestDispatcher rd = request.
                getRequestDispatcher("/jsp/Employee.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }
    
    }
    
    private void addEmployee(HttpServletRequest request,
            HttpServletResponse response) {
        try {  
            Employee employee = new Employee();
            employee.setName(request.getParameter("empName"));
            employee.setEmailId(request.getParameter("empEmailId"));
            employee.setDob(request.getParameter("dob"));
            employee.setMobileNumber(request.getParameter("mobileNumber"));
            Address address = new Address();
            address.setDoorNo(request.getParameter("doorNo"));
            address.setStreet(request.getParameter("street"));
            address.setArea(request.getParameter("area"));
            address.setDistrict(request.getParameter("district"));
            address.setPinCode(request.getParameter("pincode"));
            address.setState(request.getParameter("state"));
            employee.addAddress(address);
            employeeService.createEmployee(employee); 
            request.setAttribute("employees", employeeService.getAllEmployees());
            RequestDispatcher rd = request.
                getRequestDispatcher("/jsp/Employee.jsp");
            rd.forward(request, response);   
        } catch (ServletException e) {
            System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }   
    }
    
    private void getEmployeeToUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        try {
            Employee employee = employeeService.
                getEmployeeById(request.getParameter("empId"));
            request.setAttribute("employee", employee);   
            List<Project> projects = projectService.getAllProjects();
            projects.remove(employee.getProjects());
            request.setAttribute("projects",projects);        
            RequestDispatcher rd = request.
                getRequestDispatcher("/jsp/UpdateEmployee.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }       
    }
    
    private void updateEmployee(HttpServletRequest request,
            HttpServletResponse response) {
        try {
            Employee employee = employeeService.
            getEmployeeById(request.getParameter("empId"));
            employee.setName(request.getParameter("empName"));
            employee.setEmailId(request.getParameter("empEmailId"));
            employee.setDob(request.getParameter("dob"));
            employee.setMobileNumber(request.getParameter("mobileNumber"));
            String[] projIds = request.getParameterValues("projects"); 

            List<Project> empProjects = employee.getProjects();
            if (empProjects == null) {
                 List <Project> projsToAssign = new ArrayList<>();
            for (String projId : projIds) {
                Project project = projectService.getProjectById(projId);
                projsToAssign.add(project);
            }
            System.out.println(projsToAssign);
            employee.setProjects(projsToAssign);
            employeeService.modifyEmployee(employee);
            } else {
                List <Project> projsToAssign = new ArrayList<>();
                boolean isProjectAssigned = Boolean.FALSE;
                for (Project project : empProjects) {
                    for (String projId : projIds) {
                        if (projId.equals(project.getId())) {
                            isProjectAssigned = Boolean.TRUE;
                        }                  
                    }
                    if (!isProjectAssigned) {
                        projsToAssign.add(project);    
                    }
                    isProjectAssigned = Boolean.FALSE;
                }
                for (Project project : projsToAssign) {
                    employee.addProject(project);
                }
                employeeService.modifyEmployee(employee);
            }
            request.setAttribute("employees", employeeService.getAllEmployees());
            RequestDispatcher rd = request.
                getRequestDispatcher("/jsp/Employee.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
            System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }   
    }
    
    private void displayEmployee(HttpServletRequest request,
            HttpServletResponse response) {
        try {
            Employee employee = employeeService.
                getEmployeeById(request.getParameter("empId"));
            request.setAttribute("employee", employee); 
            request.setAttribute("projects", projectService.getAllProjects());    
            RequestDispatcher rd = request.
                getRequestDispatcher("/jsp/ShowEmployee.jsp");
            rd.forward(request, response);  
        } catch (ServletException e) {
         System.out.println("Operation failed" + e);
        } catch (IOException e) {
            System.out.println("Operation failed" + e);
        }   
    }
}
