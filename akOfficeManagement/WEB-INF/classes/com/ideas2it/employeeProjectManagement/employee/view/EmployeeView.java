package com.ideas2it.employeeProjectManagement.employee.view;

import java.util.List;
import java.util.Scanner;

import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.address.model.Address;
import com.ideas2it.employeeProjectManagement.project.model.Project;

/**
 * <p>
 * Class to interact with user and redirect flow to controller.
 * </p>
 * @author  Akshay Prabhu S R
**/
public class EmployeeView {
    private Scanner scanner = new Scanner(System.in);
    
    /**
     * <p>
     * Gets employee details from user to create/update an employee.
     * </p>
     *
     * @return employee of Employee datatype to create/update.
     */
    public Employee readEmployee(Employee employee) { 
        System.out.println("Enter Employee Name: "); 
        employee.setName(scanner.nextLine());  
        System.out.println("Enter Employee Mobile Number : ");
        employee.setMobileNumber(scanner.nextLine());   
        System.out.println("Enter Employee Email-Id : ");
        employee.setEmailId(scanner.nextLine());
        System.out.println("Enter Employee's Date of Birth (dd/mm/yyyy) :");
        employee.setDob(scanner.nextLine());
        return employee;         
    }  
    
    /**
     * <p>
     * Displays a list of employees to the user.
     * </p>
     */
    public void displayEmployees(List<Employee> employees) {
        for (Employee employee : employees) {
            System.out.println(employee.toString());
            for (Address address : employee.getAddresses()) {
                System.out.println(address.toString());
            }
            for (Project project : employee.getProjects()) {
                System.out.println(project.toString());
            }
        }
    }     
    
    /**
     * <p>
     * Gets Employee Id from the user.
     * </p>
     *
     * @return Id of employee given by user in String datatype 
     */
    public String readEmpId() { 
        System.out.println("Enter Employee Id :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Employee Id from the user, when validation fails.
     * </p>
     *
     * @return Id of employee given by user in String datatype 
     */
    public String readName() { 
        System.out.println("Enter a valid Employee Name :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Employee Id from the user.
     * </p>
     *
     * @return Id of employee given by user in String datatype 
     */
    public String readMobileNumber() { 
        System.out.println("Enter a valid Mobile Number :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Employee Id from the user.
     * </p>
     *
     * @return Id of employee given by user in String datatype 
     */
    public String readEmailId() { 
        System.out.println("Enter a valid Email-Id :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Gets Employee Id from the user.
     * </p>
     *
     * @return Id of employee given by user in String datatype 
     */
    public String readDob() { 
        System.out.println("Enter a valid Date of Birth (dd/mm/yyyy) :"); 
        return scanner.nextLine();
    }  
    
    /**
     * <p>
     * Shows a message to the user.
     * </p>
     *
     * @param msg of String datatype
     *      To be displayed to the user
     */
    public void showMsg(String msg) { 
        System.out.println(msg); 
    }  
}
    
