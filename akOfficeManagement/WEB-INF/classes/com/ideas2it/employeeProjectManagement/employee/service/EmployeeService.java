package com.ideas2it.employeeProjectManagement.employee.service;

import java.util.List;

import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;

/**
 *<p>
 * Interface to define standard operations to be performed on Employe*>/p>
 *
 * @author  Akshay Prabhu S R
**/
public interface EmployeeService {       
              
    /**
     *<p>
     * Creates and sets the values of employee details to Employee
     *</p>
     *
     * @param employee of Employee datatype
     *      Will have the employee details to be inserted
     * @return id of int datatype
     *      Will have the auto_incremented value while employee is created.
     *      If creation fails, then return 0.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    int createEmployee(Employee employee) throws ApplicationException;                         

    /**
     *<p>
     * Returns the values of all employees 
     *</p>
     *
     *@return employees of List<Employee> datatype
     *      Will contain all the employees available 
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */   
    List<Employee> getAllEmployees() throws ApplicationException;
    
    /**
     *<p>
     * Removes the Employee w.r.t empId, if it has no history of projects
 assigned else, soft deletes the employee. 
     *</p>
     *
     * @param empId with String datatype of an employee to be deleted
     *      Employee Id- will be of format "I2IT"*, where * is an integer value
     *      Example: I2IT45.
     * @return boolean value
     *      If deletion is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    boolean removeEmployeeById(String empId) throws ApplicationException;    
    
    /**
     *<p>
     * Creates and sets the values of employee details to Employee
     *</p>
     *
     * @param employee of Employee datatype
     *      Will have the employee details to be updated
     * @return boolean value
     *      If updation is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    boolean modifyEmployee(Employee employee) throws ApplicationException;
    
    /**
     *<p>
     * Separates the actual integer value of the employee that is stored in the 
 database and returns it.
     *</p>
     *
     * @param empId with String datatype
     *      Employee Id- will be of format "I2IT"*, where * is an integer value
     *      Example: I2IT45.
     * @return empNo of int datatype
     *      Will have the auto_incremented value while employee is created.
     */
    int degenerateEmpId(String empId);     
    
    /**
     *<p>
     * Validates the given employee using validation util and sets the invalid
 fields and empId as null, if any of the fields are invalid.
     *</p>
     *
     *@param employee of Employee datatype
     *      Whose values are to be validated
     *@return employee of Employee datatype
     *      Whose values are validated and set to null if invalid.
     */
    Employee validateEmployee(Employee employee);
    
    /**
     *<p>
     * Returns the values of employee w.r.t id 
     *</p>
     *
     *@param id of int datatype 
     *      Of the employee to be retrieved. 
     *@return employee of Employee datatype
     *      Will contain the employee with the given id 
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */   
    Employee getEmployeeById(String empId) throws ApplicationException;
}
