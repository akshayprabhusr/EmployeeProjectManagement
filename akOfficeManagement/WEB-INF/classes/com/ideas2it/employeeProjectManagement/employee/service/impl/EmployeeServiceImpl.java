package com.ideas2it.employeeProjectManagement.employee.service.impl;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import com.ideas2it.employeeProjectManagement.address.service.AddressService;
import com.ideas2it.employeeProjectManagement.address.service.impl.AddressServiceImpl;
import com.ideas2it.employeeProjectManagement.employee.dao.EmployeeDao;
import com.ideas2it.employeeProjectManagement.employee.dao.impl.EmployeeDaoImpl;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.employee.service.EmployeeService;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.util.DateUtil;
import com.ideas2it.employeeProjectManagement.util.ValidationUtil;

/**
 * Class to perform business logics and redirect flow to Dao layer to perform
operations on Employee.
 *
 * @author  Akshay Prabhu S R
**/
public class EmployeeServiceImpl implements EmployeeService {     
    private EmployeeDao employeeDao = new EmployeeDaoImpl();   
    private AddressService addressService = new AddressServiceImpl();
              
    /**
     * @see com.ideas2it.employeeProjectManagement.employee.service.
     *     EmployeeService #Method createEmployee(String,String,String,String)
     */
    public int createEmployee(Employee employee) throws ApplicationException { 
        return employeeDao.insertEmployee(employee);
    }                         

    /**
     * @see com.ideas2it.employeeProjectManagement.employee.service.
     *     EmployeeService #Method getAllEmployees()
     */    
    public List<Employee> getAllEmployees() throws ApplicationException {
        List<Employee> employees = new ArrayList<>(); 
        return employeeDao.retrieveAllEmployees();
    }
    
    /**
     * @see com.ideas2it.employeeProjectManagement.project.service.
     *     EmployeeService #Method getEmployeebyId()
     */    
    public Employee getEmployeeById(String empId) throws ApplicationException {
        return employeeDao.retrieveEmpById(degenerateEmpId(empId));
    }      
    
    /**
     * @see com.ideas2it.employeeProjectManagement.employee.service.
     *     EmployeeService #Method removeEmployee(String) 
     */ 
    public boolean removeEmployeeById(String empId) throws ApplicationException {
        Employee employee = getEmployeeById(empId);
        if (employee != null) {
            System.out.println(employee);
            return employeeDao.deleteEmployee(employee);
        } else {
            return Boolean.FALSE;
        } 
    }    
    
    /**
     * @see com.ideas2it.employeeProjectManagement.employee.service.
     *     EmployeeService #Method modifyEmployee(String,String,String,
     *                                            String,String)
     */
    public boolean modifyEmployee(Employee employee) throws ApplicationException {
        return employeeDao.updateEmployee(employee);     
    }
    
    /**
     * @see com.ideas2it.employeeProjectManagement.employee.service.
     *     EmployeeService #Method degeneratedEmpId(String)
     */
    public int degenerateEmpId(String empId) {
        return Integer.parseInt(empId.substring(empId.indexOf("T")+1));
    }  
    
    /**
     * @see com.ideas2it.employeeProjectManagement.employee.service.
     *     EmployeeService #Method validateEmployee(Employee)
     */
    public Employee validateEmployee(Employee employee) {
       if (!ValidationUtil.validateName(employee.getName())) {
            employee.setName(null);
            employee.setId(-1);
        }      
        if (!ValidationUtil.validateMobileNumber(employee.getMobileNumber())) {
            employee.setMobileNumber(null);
            employee.setId(-1);
        }     
        if (!ValidationUtil.validateEmailId(employee.getEmailId())) {
           employee.setEmailId(null);
           employee.setId(-1);
        }
        if (!DateUtil.validateDateFormat(employee.getDob())) {
            employee.setDob(null);
            employee.setId(-1);
        }
        if (!DateUtil.validateDate(employee.getDob()) && employee.getDob() != null) {
            employee.setDob(null);
            employee.setId(-1);
        }
        if (!(6 < DateUtil.calculateDateDiff(employee.getDob()) &&
                (DateUtil.calculateDateDiff(employee.getDob()) < 106) &&
                 employee.getDob() != null)) {
            employee.setDob(null);
            employee.setId(-1);
        }   
        return employee;
    }
}    
