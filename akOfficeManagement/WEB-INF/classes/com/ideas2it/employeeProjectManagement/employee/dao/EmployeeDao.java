package com.ideas2it.employeeProjectManagement.employee.dao;

import java.util.List;

import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;

/**
 * Interface to define standard operations to be performed on Employees table
 *
 * @author  Akshay Prabhu S R
 */
public interface EmployeeDao {
    
    /**
     *<p>
     * Inserts the values of employee in the database
     *</p>
     *
     * @param employee of Employee datatype
     *      Will have the details to be inserted
     * @return employee of Employee datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting an employee into the datatype if inserted 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */
    int insertEmployee(Employee employee) throws ApplicationException;

    /**
     *<p>
     * Soft deletes an employee from the database by setting isDeleted as "Yes"
     *</p>
     *
     * @param id of int datatype
     *      Will have the auto_generated key for the employee to be deleted
     * @return boolean value
     *      If deletion is successful, then Boolean.TRUE, else Boolean.FALSE.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */  
    boolean deleteEmployee(Employee employee) throws ApplicationException;
    
    /**
     *<p>
     * Retrieves an employee from the database w.r.t empNo
     *</p>
     *
     * @param id of int datatype
     *      Will have the auto_generated key for the employee to be retrieved
     * @return employee of Employee datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting an employee into the database if retrieved 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */  
    Employee retrieveEmpById(int id) throws ApplicationException;

    /**
     *<p>
     * Retrieves the values of all employees 
     *</p>
     *
     *@return employees of List<Employee> datatype
     *      Will contain all the employees available 
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */  
    List<Employee> retrieveAllEmployees() throws ApplicationException;
 
    /**
     *<p>
     * Updates the values of employee in the database
     *</p>
     *
     * @param employee of Employee datatype
     *      Will have the details to be updated
     * @return employee of Employee datatype
     *      Will have all the inserted values and the auto_generated value
     *      while inserting an employee into the database if inserted 
     *      successfully, else null.
     * @throws ApplicationException, if there is any problem with the connection
     *      to the database or if there is any faults in the query executed.
     */ 
    boolean updateEmployee(Employee employee) throws ApplicationException;
}
