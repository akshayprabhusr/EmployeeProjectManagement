package com.ideas2it.employeeProjectManagement.employee.dao.impl;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException; 
import org.hibernate.criterion.Restrictions;
import org.hibernate.Session; 
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ideas2it.employeeProjectManagement.common.Constants;
import com.ideas2it.employeeProjectManagement.employee.dao.EmployeeDao;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.employee.service.EmployeeService;
import com.ideas2it.employeeProjectManagement.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import hibernate.HibernateSessionFactory;
import logger.LogManager;
/**
 * Class to implement EmployeeDao and perform database operations for Employee
from Employees table.
 *
 * @author  Akshay Prabhu S R
**/
public class EmployeeDaoImpl implements EmployeeDao {
    private  HibernateSessionFactory hibernateSessionFactory = 
        new HibernateSessionFactory();
    private SessionFactory sessionFactory;

    /**
     * @see com.ideas2it.employeeProjectManagement.employee.Dao.
     *     EmployeeDao #Method insertEmployee(Employee)
     */
    public int insertEmployee(Employee employee) 
            throws ApplicationException {
        Session session = null;
        try {
            sessionFactory =  hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            
            int id = (Integer) session.save(employee);
            tx.commit();
            return id;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_EMP_CR + employee, e);
            throw new ApplicationException(Constants.OP_FAILED);
        }  
        finally {
            if(session!=null) {
                session.close();
            }           
             
        }                	
    }
    
    /**
     * @see com.ideas2it.employeeProjectManagement.employee.Dao.
     *     EmployeeDao #Method deleteEmployee(id)
     */
    public boolean deleteEmployee(Employee employee) throws ApplicationException {
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            employee.setIsDeleted(Boolean.TRUE);
            session.update(employee);
            tx.commit();
            return Boolean.TRUE;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_EMP_DEL + employee.getId(), e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }     
        finally {
           session.close(); 
        }
    }   

    /**
     * @see com.ideas2it.employeeProjectManagement.employee.Dao.
     *     EmployeeDao #Method retrieveEmployee(int)
     */
    public Employee retrieveEmpById(int id) throws ApplicationException{
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Employee.class)
                .add(Restrictions.eq("id", id));
            Employee employee = (Employee) criteria.uniqueResult();
            tx.commit();
            return employee;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_EMP_RET +  id, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }
        finally {
            session.close(); 
        }        
    }

    /**
     * @see com.ideas2it.employeeProjectManagement.employee.Dao.
     *     EmployeeDao #Method retrieveAllEmployees()
     */
    public List<Employee> retrieveAllEmployees() throws ApplicationException {
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(Employee.class);
            criteria.add(Restrictions.eq("isDeleted", Boolean.FALSE));
            List<Employee> employees = criteria.list(); 
            tx.commit();
            return employees;
        } catch (HibernateException e) {
            e.printStackTrace();
            LogManager.handleError(Constants.EX_EMPS_RET, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }
        finally {
           session.close(); 
        }
    }    
 
    /**
     * @see com.ideas2it.employeeProjectManagement.employee.Dao.
     *     EmployeeDao #Method updateEmployee(Employee) 
     */
    public boolean updateEmployee(Employee employee) 
            throws ApplicationException {
        Session session = null;
        try {
            sessionFactory = hibernateSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            session.clear();
            session.update(employee);
            tx.commit();
            return Boolean.TRUE;
        } catch (HibernateException e) {
            LogManager.handleError(Constants.EX_EMP_UPD + employee, e); 
            throw new ApplicationException(Constants.OP_FAILED);
        }  
        finally {
            session.close(); 
        }
    }
}
