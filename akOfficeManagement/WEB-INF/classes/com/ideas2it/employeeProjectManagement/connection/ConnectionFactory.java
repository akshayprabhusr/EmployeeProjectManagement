package com.ideas2it.employeeProjectManagement.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
 
public class ConnectionFactory {
  
    /**
     * Class to contain methods that are required to operate with database
     * @author Akshay Prabhu S R
     */ 
    private static final String URL = "jdbc:mysql://localhost:3306/" +
        "EmployeeProjectManagement?autoReconnect=true&useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "root";
        
    public ConnectionFactory() {
    }
     
    /**
     * Establishes connection with the database
     */
    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException exception) {
            System.out.println("Unable to Connect to Database");
        }
        return connection;
    }    

    /**
     * Closes Connection,ResultSet and PreparedStatement at the end
     */
    public void closeAll(ResultSet rs, PreparedStatement ps, Connection con ) {
        if (rs != null) {
            try {
                rs.close();
            } 
            catch (SQLException e) { 
            }
        }
        if (ps != null) {
            try {
                ps.close();
            } 
            catch (SQLException e) { 
            }
        }   
        if (con != null) {
            try {
                con.close();
            }
            catch (SQLException e) { 
            }
        }
    }  
}
