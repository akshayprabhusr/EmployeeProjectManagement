package com.ideas2it.employeeProjectManagement.util;

import java.util.regex.Pattern;

/**
 * Class to contain validation methods and perform validation
 * @author Akshay Prabhu S R
 */ 
public class ValidationUtil {
     
    /**
     * <p>
     * Validates name with respect to a regular expression containing only 
 alphabets of upper and lower cases.
     * </p>
     *
     * @param name with String datatype
     *      Can contain only a-z, A-Z and spaces. 
     * @return boolean value true if validation else false  
     */
    public static boolean validateName(String name) {
        return Pattern.matches("[a-z A-Z]{1,}", name);             
    }

    /**
     * <p>
     * Validates mobileNumber with respect to a regular expression containing 
 only 10 digits of specific pattern.
     * </p>
     *
     * @param mobileNumber with String datatype
     *      Should start with 9/8/7 , has 10 digits. Example: 9566786178
     * @return boolean value true if validation else false     
     */
    public static boolean validateMobileNumber(String mobileNumber) {
        return Pattern.matches("[789]{1}[0-9]{9}", mobileNumber);
    }            

    /**
     * <p>
     * Validates emailId with respect to a regular expression containing 
 alphabets and symbols in a specific pattern.
     * </p>
     *
     * @param emailId with String datatype
     *      Should follow a regular expression where @ and . are manditory in 
     *      between the strings. Example: akshay@gmail.com is valid where
     *      akshay@.com, @gmail.com, akshay@gmail. are invalid.
     * @return boolean value true if validation else false 
     */
    public static boolean validateEmailId(String emailId) {
        return Pattern.matches("\\S*[^@]" + "[@]{1}" + "[a-zA-Z]{1,}" +
                "[.]{1}" + "[a-zA-Z]{1,}", emailId);              
    }

    /**
     * <p>
     * Validates line with respect to a regular expression containing only 
 alphabets of upper case, lower case and periods.
     * </p>
     *
     * @param line with String datatype
     *      Can contain only a-z, A-Z and spaces.     
     * @return boolean value true if validation else false  
     */
    public static boolean validateLine(String line) {        
        return Pattern.matches("[a-zA-Z0-9 ]{1,}",line);   
    }            
    
    /**
     * <p>
     * Validates line with respect to a regular expression containing only 
 alphabets of upper case, lower case, digits and periods.
     * </p>
     *
     * @param line with String datatype
     *      Can contain only a-z, A-Z and spaces.     
     * @return boolean value true if validation else false  
     */
    public static boolean validateAlphaLine(String alphaLine) {        
        return Pattern.matches("[a-z A-Z]{1,}",alphaLine);   
    }    
    
    /**
     * <p>
     * Validates pinCode with respect to a regular expression containing 
 only 6 digits of specific pattern.
     * </p>
     *
     * @param pinCode with String datatype
     *      Should have 6 digits only, without 0 as first digit. Example:641002
     * @return boolean value true if validation else false     
     */
    public static boolean validatePinCode(String pinCode) {
        return Pattern.matches("[1-9]{1}[0-9]{5}", pinCode);
    }                  
}
