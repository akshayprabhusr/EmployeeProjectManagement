package com.ideas2it.employeeProjectManagement.util;

import java.lang.Boolean;
import java.util.Calendar;
import java.util.regex.Pattern;

/**
 * Class to contain validation methods and perform validation
 *
 * @author Akshay Prabhu S R
 */ 
public class DateUtil {    
    /**
     * Validates Year of birth
     *
     * @param date with String datatype
     *      Should follow a regular expression to contain a format dd/mm/yyyy
     *      Example: 20/04/1996.    
     * @return boolean value Boolean.TRUE if validation else Boolean.FALSE  
     */
    public static boolean validateDateFormat(String date) {
        return Pattern.matches("[0-9]{2}/[0-9]{2}/[0-9]{4}", date);
    }

    /**
     * Validates Date with respect to year,month and day
     *
     * @param dateAsString with String datatype
     *      Will follow a regular expression to contain a format dd/mm/yyyy
     *      Example: 20/04/1996 is valid, whereas 30/02/1996, 10/20/2000 and 
     *      31/04/1996 are invalid.
     * @return boolean value Boolean.TRUE if valid else Boolean.FALSE  
     */
    public static boolean validateDate(String dateAsString) {
        String date[] = dateAsString.split("/");
        int day = Integer.parseInt(date[0]);
        int month = Integer.parseInt(date[1]);
        int year = Integer.parseInt(date[2]);        
        if (month > 13) { 
            return Boolean.FALSE;
        } else {
            if (month == 2) {
                if (year % 4 == 0) {
                    return day < 30 ? Boolean.TRUE : Boolean.FALSE;
                } else { 
                    return day < 29 ? Boolean.TRUE : Boolean.FALSE;
                } 
            }     
            else {
                if (month == 4 || month == 6 || month == 9 || month ==11) {
                    return day < 31 ? Boolean.TRUE : Boolean.FALSE;
                } else { 
                    return day < 32 ? Boolean.TRUE : Boolean.FALSE;
                }
            }            
        }
    } 
    
    /**
     * Calculates the difference in date w.r.t dataAsString.
     *
     * @param dateAsString with String datatype
     *      Will follow a regular expression to contain a format dd/mm/yyyy
     *      Example: 20/04/1996 
     * @return differenceInDate with int datatype
     *      Will return the difference in the passed date with current date. 
     */
    public static int calculateDateDiff(String dateAsString) {       
        Calendar today = Calendar.getInstance();
        String date[] = dateAsString.split("/");
        int day = Integer.parseInt(date[0]);
        int month = Integer.parseInt(date[1]);
        int year = Integer.parseInt(date[2]);
        int differenceInDate = today.get(Calendar.YEAR) - year ;
        if (month > today.get(Calendar.MONTH)) { 
            differenceInDate --;
        } else if (month== today.get(Calendar.MONTH)) { 
            if (day > today.get(Calendar.DAY_OF_MONTH) ) { 
                differenceInDate --;
            }
        }
        return differenceInDate;        
    }    
}
