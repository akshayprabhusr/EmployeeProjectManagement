package com.ideas2it.employeeProjectManagement.exception;

import java.lang.RuntimeException;

public class ApplicationException extends RuntimeException {
    public ApplicationException() {
    }
    
    public ApplicationException(String msg) {
        super(msg);
    }
    
    public ApplicationException(Throwable cause) {
        super(cause);
    }
    
    public ApplicationException(String msg, Throwable cause) {
        super(msg, cause);
    }
    
    public ApplicationException(String msg, Throwable cause, 
            boolean enableSuppresion, boolean writableStackTrace) {
        super(msg, cause, enableSuppresion, writableStackTrace);
    }
}
    
