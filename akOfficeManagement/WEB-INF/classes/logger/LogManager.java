package logger;

import java.lang.Throwable;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * <p>
 * Class to perform logger operations generically for employeeProjectManagement
 * </p>
 *
 * @author Akshay Prabhu S R
**/ 
public class LogManager {
    private static Logger logger;
    
    public static void configureLog4j() {
        String log4jConfigFile ="/home/ubuntu/Desktop/ak/apache-tomcat-7.0.81/webapps/akOfficeManagement/WEB-INF/classes/logger/loggerConfig.xml";
        DOMConfigurator.configure(log4jConfigFile);
    }
    
    private static Logger getMyLogger(String className) {
        configureLog4j();
        return Logger.getLogger(className);
    }
    
    public static void handleError(String str, Throwable cause) {
        logger = getMyLogger(cause.getStackTrace()[0].getClassName());
        logger.error(str, cause);
    }
    
    public static void handleDebug(String str, Throwable cause) {
        logger = getMyLogger(cause.getStackTrace()[0].getClassName());
        logger.debug(str, cause);
    }
    
    public static void handleInfo(String str, Throwable cause) {
        logger = getMyLogger(cause.getStackTrace()[0].getClassName());
        logger.info(str, cause);
    }
    
    public static void handleFatal(String str, Throwable cause) {
        logger = getMyLogger(cause.getStackTrace()[0].getClassName());
        logger.fatal(str, cause);
    }      
}
    
    
