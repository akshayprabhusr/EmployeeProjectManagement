
import java.lang.Boolean;
import java.sql.SQLException;
import java.util.Scanner;

import org.apache.log4j.xml.DOMConfigurator;

import com.ideas2it.employeeProjectManagement.address.controller.AddressController;
import com.ideas2it.employeeProjectManagement.client.controller.ClientController;
import com.ideas2it.employeeProjectManagement.client.service.ClientService;
import com.ideas2it.employeeProjectManagement.client.service.impl.ClientServiceImpl;
import com.ideas2it.employeeProjectManagement.common.Constants;
import com.ideas2it.employeeProjectManagement.employee.controller.EmployeeController;
import com.ideas2it.employeeProjectManagement.employee.model.Employee;
import com.ideas2it.employeeProjectManagement.exception.ApplicationException;
import com.ideas2it.employeeProjectManagement.project.controller.ProjectController;
import com.ideas2it.employeeProjectManagement.project.model.Project;
import com.ideas2it.employeeProjectManagement.util.DateUtil;
import logger.LogManager;

/**
 * <p>
 * Class to interact with user and perform operations via controllers.
 Acts as a view for user and to interact with user via list of choices 
 * </p>
 *
 * @author  Akshay Prabhu S R 
**/
public class EmployeeProjectApplication {  
    private AddressController addressController;
    private ClientController clientController;
    private EmployeeController employeeController;
    private ProjectController projectController;      
                               
    public static void main(String args[]) {
        EmployeeProjectApplication employeeProjectApplication = 
            new EmployeeProjectApplication();        
	    employeeProjectApplication.startApplication();	           
    }

    /**
     * <p>
     * Displays a main menu for the user to select a choice of operation to
 perform on employee, project, client and address.
     *</p>
     *  
     */
    private void startApplication() {
        boolean willExitSwitch = Boolean.FALSE;            
        int choiceMade; 
        Scanner scanner = new Scanner(System.in);
        MainMenu:
        do {
            try {
                System.out.println(Constants.MAIN_MENU);     
                choiceMade = Integer.parseInt(scanner.nextLine());
                switch(choiceMade) {
                    case 1: 
                        employeeController = new EmployeeController();
                       // employeeController.manageEmployee();
                        break;
                    case 2:
                        projectController= new ProjectController();
                      //  projectController.manageProject();                    
                        break;
                    case 3:
                        clientController = new ClientController();
                       // clientController.manageClient(); 
                        break;     
                    case 4:
                        addressController = new AddressController();
                      //  addressController.manageAddress();
                        break;
                    case 5:
                        willExitSwitch = Boolean.TRUE;
                        break;
                    default:
                        System.out.println(Constants.INVALID_IP);
                        break;
                }
            } catch (NumberFormatException e) {
                System.out.println(Constants.INVALID_IP);
            }
            finally {
                continue MainMenu;
            }
        } while (!willExitSwitch);
              
    }
}                               
